# The module defines the following variables:
#   GPERF_EXECUTABLE - path to gperf command line client
#   GPERF_FOUND - true if the command line client was found
# Example usage:
#   find_package(Git)
#   if(GPERF_FOUND)
#     message("gperf found: ${GPERF_EXECUTABLE}")
#   endif()

#=============================================================================
# Copyright 2010 Kitware, Inc.
# Copyright 2012 Rolf Eike Beer <eike@sf-mail.de>
#
# Distributed under the OSI-approved BSD License (the "License");
# see accompanying file Copyright.txt for details.
#
# This software is distributed WITHOUT ANY WARRANTY; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the License for more information.
#=============================================================================
# (To distribute this file outside of CMake, substitute the full
#  License text for the above reference.)


find_program(GPERF_EXECUTABLE
  gperf
  DOC "gperf executable"
  )
mark_as_advanced(GPERF_EXECUTABLE)

# Handle the QUIETLY and REQUIRED arguments and set GPERF_FOUND to TRUE if
# all listed variables are TRUE

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(Gperf
                                  REQUIRED_VARS GPERF_EXECUTABLE)
