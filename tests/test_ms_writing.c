#define _POSIX_C_SOURCE 200809L
#include <stdio.h>
#include <stdlib.h>
#ifndef _WIN32
#include <unistd.h>
#else
#include <io.h>
#endif
#include <check.h>
#include <errno.h>

#include "atom_group.h"
#include "pdb.h"
#include "ms.h"
#include "sasa.h"

START_TEST(test_write_ms)
{
	struct mol_atom_group *ag = mol_read_ms("test.ms");
	char fname[] = "tempfileXXXXXX";
#ifdef _WIN32
	char* tmp_filename = mktemp(fname);
	FILE *tmpfile = fopen(tmp_filename, "w+");
#else
	int tmp_file_descriptor = mkstemp(fname);
	ck_assert(tmp_file_descriptor != -1);
	FILE *tmpfile = fdopen(tmp_file_descriptor, "w+");
#endif /* _WIN32 */
	ck_assert(tmpfile != NULL);

	mol_fwrite_ms(tmpfile, ag);
	mol_atom_group_free(ag);

	rewind(tmpfile);
	ag = mol_fread_ms(tmpfile);
	fclose(tmpfile);
#ifndef _WIN32
	close(tmp_file_descriptor);
#endif /* _WIN32 */
	ck_assert(ag != NULL);
	ck_assert_int_eq(ag->natoms, 4021);
	//ATOM      2  CA  GLU H   0      -9.966  14.057  59.651   9.9
	ck_assert_str_eq(ag->atom_name[1], " CA ");
	ck_assert(ag->alternate_location[1] == ' ');
	ck_assert_str_eq(ag->residue_name[1], "GLU");
	ck_assert(ag->residue_id[1].chain == 'H');
	ck_assert_int_eq(ag->residue_id[1].residue_seq, 0);
	ck_assert(ag->residue_id[1].insertion == ' ');
	ck_assert(ag->coords[1].X == -9.966);
	ck_assert(ag->coords[1].Y == 14.057);
	ck_assert(ag->coords[1].Z == 59.651);
	ck_assert(ag->attraction_level[1] == 9.9);
	ck_assert(ag->surface[1] == true);

	ck_assert(ag->attraction_level[2] == 0.0);
	ck_assert(ag->surface[2] == false);

	for (size_t i = 0; i < ag->natoms; ++i) {
		struct mol_residue *res = find_residue(ag,
						ag->residue_id[i].chain,
						ag->residue_id[i].residue_seq,
						ag->residue_id[i].insertion);
		ck_assert(res != NULL);
		ck_assert(i >= res->atom_start);
		ck_assert(i <= res->atom_end);
	}

	unlink(fname);
	mol_atom_group_free(ag);
}
END_TEST

Suite *atom_group_suite(void)
{
	Suite *suite = suite_create("ms_writing");

	TCase *tcase_write = tcase_create("test_write");
	tcase_add_test(tcase_write, test_write_ms);

	suite_add_tcase(suite, tcase_write);

	return suite;
}

int main(void)
{
	Suite *suite = atom_group_suite();
	SRunner *runner = srunner_create(suite);
	srunner_run_all(runner, CK_ENV);

	int number_failed = srunner_ntests_failed(runner);
	srunner_free(runner);
	return number_failed;
}
