#include <stdio.h>
#include <check.h>
#include <errno.h>
#include <float.h>
#include <math.h>

#include "atom_group.h"
#include "pdb.h"

START_TEST(test_diameter)
{
	struct mol_atom_group *ag = mol_read_pdb("triple.pdb");
	double diameter = mol_atom_group_diameter(ag);
	ck_assert(fabs(diameter - 7.0) <= DBL_EPSILON);
	mol_atom_group_free(ag);
}
END_TEST

Suite *atom_group_suite(void)
{
	Suite *suite = suite_create("atom_group");

	TCase *tcases = tcase_create("test_diameter");
	tcase_add_test(tcases, test_diameter);

	suite_add_tcase(suite, tcases);

	return suite;
}

int main(void)
{
	Suite *suite = atom_group_suite();
	SRunner *runner = srunner_create(suite);
	srunner_run_all(runner, CK_ENV);

	int number_failed = srunner_ntests_failed(runner);
	srunner_free(runner);
	return number_failed;
}
