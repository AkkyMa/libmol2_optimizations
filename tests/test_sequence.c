#include <stdio.h>
#include <string.h>
#include <check.h>
#include <errno.h>

#include "sequence.h"
#include "pdb.h"

START_TEST(test_seq)
{
	struct mol_atom_group *ag = mol_read_pdb("seq.pdb");
	char *seq = mol_atom_group_sequence(ag);
	printf("%s\n", seq);
	ck_assert(strcmp(seq, "ACDEFGHIKLMNPQRSTVWY/ACDEFGHIKLMNPQRSTVWY") == 0);
	free(seq);
	mol_atom_group_free(ag);
}
END_TEST

START_TEST(test_seqres)
{
	struct mol_atom_group *ag = mol_read_pdb("seq.pdb");
	char *seq = ag->seqres;
	printf("%s\n", seq);
	ck_assert(strcmp(seq, "ACDEFGHIKLMNPQRSTVWY/ACDEFGHIKLMNPQRSTVWY") == 0);
	mol_atom_group_free(ag);
}
END_TEST

Suite *sequence_suite(void)
{
	Suite *suite = suite_create("seq");

	TCase *tcase_seq = tcase_create("test_seq");
	tcase_add_test(tcase_seq, test_seq);
	tcase_add_test(tcase_seq, test_seqres);

	suite_add_tcase(suite, tcase_seq);

	return suite;
}

int main(void)
{
	Suite *suite = sequence_suite();
	SRunner *runner = srunner_create(suite);
	srunner_run_all(runner, CK_ENV);

	int number_failed = srunner_ntests_failed(runner);
	srunner_free(runner);
	return number_failed;
}
