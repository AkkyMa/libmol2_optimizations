#include <stdio.h>
#include <check.h>
#include <errno.h>

#include "atom_group.h"
#include "pdb.h"

static bool comp_element(const struct mol_atom_group *ag,
			 size_t index,
			 void *params)
{
	const char *elem = (char *) params;
	return (strcmp(ag->element[index], elem) == 0);
}

static bool comp_atom_name(const struct mol_atom_group *ag,
			 size_t index,
			 void *params)
{
	const char *atom_name = (char *) params;
	return (strcmp(ag->atom_name[index], atom_name) == 0);
}


START_TEST(test_ex)
{
	struct mol_atom_group *ag = mol_read_pdb("1rei.pdb");
	ck_assert(ag != NULL);
	ck_assert_int_eq(ag->natoms, 1707);

	struct mol_atom_group *only_CA = mol_extract_atom_type(ag, "CA");
	ck_assert_int_eq(only_CA->natoms, 214);
	for (size_t i = 0; i < only_CA->natoms; ++i) {
		ck_assert_str_eq(only_CA->atom_name[i], " CA ");
	}
	mol_atom_group_free(ag);
	mol_atom_group_free(only_CA);
}
END_TEST

START_TEST(test_ex2)
{
	struct mol_atom_group *ag = mol_read_pdb("1rei.pdb");
	ck_assert(ag != NULL);
	ck_assert_int_eq(ag->natoms, 1707);

	struct mol_atom_group *only_C = mol_extract_atom_type(ag, " C  ");
	ck_assert_int_eq(only_C->natoms, 214);
	for (size_t i = 0; i < only_C->natoms; ++i) {
		ck_assert_str_eq(only_C->atom_name[i], " C  ");
	}
	mol_atom_group_free(ag);
	mol_atom_group_free(only_C);
}
END_TEST

START_TEST(test_ex3)
{
	struct mol_atom_group *ag = mol_read_pdb("1rei.pdb");
	ck_assert(ag != NULL);
	ck_assert_int_eq(ag->natoms, 1707);

	struct mol_list types = MOL_STR_LIST_CREATE("C", "CA");

	struct mol_atom_group *only_C_CA = mol_extract_atom_types(ag, &types);
	ck_assert_int_eq(only_C_CA->natoms, 428);
	for (size_t i = 0; i < only_C_CA->natoms; ++i) {
		ck_assert(comp_atom_name(only_C_CA, i, " C  ") ||
			  comp_atom_name(only_C_CA, i, " CA "));
	}
	mol_atom_group_free(ag);
	mol_atom_group_free(only_C_CA);
}
END_TEST

START_TEST(test_ex4)
{
	struct mol_atom_group *ag = mol_read_pdb("1rei.pdb");
	ck_assert(ag != NULL);
	ck_assert_int_eq(ag->natoms, 1707);

	struct mol_list types = MOL_STR_LIST_CREATE(" C  ", " CA ");

	struct mol_atom_group *only_C_CA = mol_extract_atom_types(ag, &types);
	ck_assert_int_eq(only_C_CA->natoms, 428);
	for (size_t i = 0; i < only_C_CA->natoms; ++i) {
		ck_assert(comp_atom_name(only_C_CA, i, " C  ") ||
			  comp_atom_name(only_C_CA, i, " CA "));
	}
	mol_atom_group_free(ag);
	mol_atom_group_free(only_C_CA);
}
END_TEST

START_TEST(test_rm)
{
	struct mol_atom_group *ag = mol_read_pdb("1rei.pdb");
	ck_assert(ag != NULL);

	struct mol_atom_group *no_CA = mol_remove_atom_type(ag, "CA");
	ck_assert_int_eq(no_CA->natoms, 1493);
	for (size_t i = 0; i < no_CA->natoms; ++i) {
		ck_assert_str_ne(no_CA->atom_name[i], " CA ");
	}
	mol_atom_group_free(ag);
	mol_atom_group_free(no_CA);
}
END_TEST

START_TEST(test_extract_chain)
{
	struct mol_atom_group *ag = mol_read_pdb("1rei.pdb");
	ck_assert(ag != NULL);
	ck_assert_int_eq(ag->natoms, 1707);

	struct mol_atom_group *only_B = mol_extract_chain(ag, 'B');
	ck_assert_int_eq(only_B->natoms, 851);
	for (size_t i = 0; i < only_B->natoms; ++i) {
		ck_assert(only_B->residue_id[i].chain == 'B');
	}
	mol_atom_group_free(ag);
	mol_atom_group_free(only_B);
}
END_TEST

START_TEST(test_rm2)
{
	struct mol_atom_group *ag = mol_read_pdb("1rei.pdb");
	ck_assert(ag != NULL);

	struct mol_atom_group *no_C = mol_remove_atom_type(ag, " C  ");
	ck_assert_int_eq(no_C->natoms, 1493);
	for (size_t i = 0; i < no_C->natoms; ++i) {
		ck_assert_str_ne(no_C->atom_name[i], " C  ");
	}
	mol_atom_group_free(ag);
	mol_atom_group_free(no_C);
}
END_TEST

START_TEST(test_select_property1)
{
	struct mol_atom_group *ag = mol_read_pdb("1rei.pdb");
	ck_assert(ag != NULL);

	struct mol_atom_group *only_backbone =
		mol_atom_group_select(ag, mol_is_backbone, NULL);
	ck_assert_int_eq(only_backbone->natoms, 856);
	for (size_t i = 0; i < only_backbone->natoms; ++i) {
		ck_assert(mol_is_backbone(only_backbone, i, NULL));
	}
	mol_atom_group_free(ag);
	mol_atom_group_free(only_backbone);
}
END_TEST

START_TEST(test_select_property2)
{
	struct mol_atom_group *ag = mol_read_pdb("1rei.pdb");
	ck_assert(ag != NULL);

	struct mol_atom_group *only_sidechain =
		mol_atom_group_select(ag, mol_is_sidechain, NULL);
	ck_assert_int_eq(only_sidechain->natoms, 796);
	for (size_t i = 0; i < only_sidechain->natoms; ++i) {
		ck_assert(mol_is_sidechain(only_sidechain, i, NULL));
	}
	mol_atom_group_free(ag);
	mol_atom_group_free(only_sidechain);
}
END_TEST

START_TEST(test_filter_property1)
{
	struct mol_atom_group *ag = mol_read_pdb("1rei.pdb");
	ck_assert(ag != NULL);
	ck_assert(ag->element != NULL);

	struct mol_atom_group *no_nitrogens =
		mol_atom_group_filter(ag, comp_element, " N");
	ck_assert_int_eq(no_nitrogens->natoms, 1441);
	for (size_t i = 0; i < no_nitrogens->natoms; ++i) {
		ck_assert(!comp_element(no_nitrogens, i, " N"));
	}
	mol_atom_group_free(ag);
	mol_atom_group_free(no_nitrogens);
}
END_TEST

START_TEST(test_test_foreach)
{
	struct mol_atom_group *ag = mol_read_pdb("1rei.pdb");
	ck_assert(ag != NULL);
	ck_assert(ag->element != NULL);

	bool *is_N =
		mol_atom_group_test_foreach(ag, comp_element, " N");

	for (size_t i = 0; i < ag->natoms; ++i) {
		ck_assert(is_N[i] == comp_element(ag, i, " N"));
	}
	mol_atom_group_free(ag);
	free(is_N);
}
END_TEST

START_TEST(test_extract_prefix)
{
	struct mol_atom_group *ag = mol_read_pdb("heparin.pdb");
	ck_assert(ag != NULL);
	ck_assert_int_eq(ag->natoms, 79);

	struct mol_atom_group *only_CA = mol_extract_atom_type_prefix(ag, "CA");
	ck_assert_int_eq(only_CA->natoms, 4);
	for (size_t i = 0; i < only_CA->natoms; ++i) {
		ck_assert(strncmp(only_CA->atom_name[i], " CA", 3) == 0);
	}
	mol_atom_group_free(ag);
	mol_atom_group_free(only_CA);
}
END_TEST

Suite *atom_group_suite(void)
{
	Suite *suite = suite_create("atom_group");

	TCase *tcases = tcase_create("test_exrm");
	tcase_add_test(tcases, test_ex);
	tcase_add_test(tcases, test_rm);
	tcase_add_test(tcases, test_rm2);
	tcase_add_test(tcases, test_extract_chain);
	tcase_add_test(tcases, test_ex2);
	tcase_add_test(tcases, test_ex3);
	tcase_add_test(tcases, test_ex4);
	tcase_add_test(tcases, test_extract_prefix);
	tcase_add_test(tcases, test_select_property1);
	tcase_add_test(tcases, test_select_property2);
	tcase_add_test(tcases, test_filter_property1);
	tcase_add_test(tcases, test_test_foreach);

	suite_add_tcase(suite, tcases);

	return suite;
}

int main(void)
{
	Suite *suite = atom_group_suite();
	SRunner *runner = srunner_create(suite);
	srunner_run_all(runner, CK_ENV);

	int number_failed = srunner_ntests_failed(runner);
	srunner_free(runner);
	return number_failed;
}
