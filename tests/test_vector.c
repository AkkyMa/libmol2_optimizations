#include <stdio.h>
#include <check.h>
#include <errno.h>
#include <float.h>
#include <math.h>

#include "vector.h"

START_TEST(test_add)
{
  struct mol_vector3 x = {1.0, 1.0, 1.0};
  struct mol_vector3 y = {2.0, 2.5, -1.0};
  struct mol_vector3 dst;

  MOL_VEC_ADD(dst, x, y);

  ck_assert(fabs(dst.X - 3.0) <= DBL_EPSILON);
  ck_assert(fabs(dst.Y - 3.5) <= DBL_EPSILON);
  ck_assert(fabs(dst.Z) <= DBL_EPSILON);
}
END_TEST

START_TEST(test_subtract)
{
  struct mol_vector3 x = {1.0, 1.0, 1.0};
  struct mol_vector3 y = {2.0, 2.5, -1.0};
  struct mol_vector3 dst;

  MOL_VEC_SUB(dst, x, y);

  ck_assert(fabs(dst.X - (-1.0)) <= DBL_EPSILON);
  ck_assert(fabs(dst.Y - (-1.5)) <= DBL_EPSILON);
  ck_assert(fabs(dst.Z - 2.0) <= DBL_EPSILON);
}
END_TEST


START_TEST(test_multiply)
{
  struct mol_vector3 x = {1.0, 1.0, 1.0};
  struct mol_vector3 y = {2.0, 2.5, -1.0};
  struct mol_vector3 dst;

  MOL_VEC_MULT(dst, x, y);

  ck_assert(fabs(dst.X - 2.0) <= DBL_EPSILON);
  ck_assert(fabs(dst.Y - 2.5) <= DBL_EPSILON);
  ck_assert(fabs(dst.Z - (-1.0)) <= DBL_EPSILON);
}
END_TEST


START_TEST(test_divide)
{
  struct mol_vector3 x = {1.0, 1.0, 1.0};
  struct mol_vector3 y = {2.0, 2.5, -1.0};
  struct mol_vector3 dst;

  MOL_VEC_DIV(dst, x, y);

  ck_assert(fabs(dst.X - 0.5) <= DBL_EPSILON);
  ck_assert(fabs(dst.Y - 0.4) <= DBL_EPSILON);
  ck_assert(fabs(dst.Z - (-1.0)) <= DBL_EPSILON);
}
END_TEST

START_TEST(test_dot_prod)
{
  struct mol_vector3 x = {1.0, 1.0, 1.0};
  struct mol_vector3 y = {2.0, 2.5, -1.0};

  double dot_prod = MOL_VEC_DOT_PROD(x, y);

  ck_assert(fabs(dot_prod - (3.5)) <= DBL_EPSILON);
}
END_TEST

START_TEST(test_cross_prod)
{
  struct mol_vector3 x = {1.0, 1.0, 1.0};
  struct mol_vector3 y = {2.0, 2.5, -1.0};
  struct mol_vector3 dst;

  MOL_VEC_CROSS_PROD(dst, x, y);

  ck_assert(fabs(dst.X - (-3.5)) <= DBL_EPSILON);
  ck_assert(fabs(dst.Y - 3.0) <= DBL_EPSILON);
  ck_assert(fabs(dst.Z - 0.5) <= DBL_EPSILON);
}
END_TEST

START_TEST(test_add_scalar)
{
  struct mol_vector3 x = {1.0, 2.0, 3.0};
  struct mol_vector3 dst;

  MOL_VEC_ADD_SCALAR(dst, x, 3.0-2.0);

  ck_assert(fabs(dst.X - 2.0) <= DBL_EPSILON);
  ck_assert(fabs(dst.Y - 3.0) <= DBL_EPSILON);
  ck_assert(fabs(dst.Z - 4.0) <= DBL_EPSILON);
}
END_TEST

START_TEST(test_subtract_scalar)
{
  struct mol_vector3 x = {1.0, 2.0, 3.0};
  struct mol_vector3 dst;

  MOL_VEC_SUB_SCALAR(dst, x, 3.0-2.0);

  ck_assert(fabs(dst.X - 0.0) <= DBL_EPSILON);
  ck_assert(fabs(dst.Y - 1.0) <= DBL_EPSILON);
  ck_assert(fabs(dst.Z - 2.0) <= DBL_EPSILON);
}
END_TEST


START_TEST(test_multiply_scalar)
{
  struct mol_vector3 x = {1.0, 2.0, 3.0};
  struct mol_vector3 dst;

  MOL_VEC_MULT_SCALAR(dst, x, 4.0-2.0);

  ck_assert(fabs(dst.X - 2.0) <= DBL_EPSILON);
  ck_assert(fabs(dst.Y - 4.0) <= DBL_EPSILON);
  ck_assert(fabs(dst.Z - 6.0) <= DBL_EPSILON);
}
END_TEST


START_TEST(test_divide_scalar)
{
  struct mol_vector3 x = {1.0, 2.0, 3.0};
  struct mol_vector3 dst;

  MOL_VEC_DIV_SCALAR(dst, x, 4.0-2.0);

  ck_assert(fabs(dst.X - 0.5) <= DBL_EPSILON);
  ck_assert(fabs(dst.Y - 1.0) <= DBL_EPSILON);
  ck_assert(fabs(dst.Z - 1.5) <= DBL_EPSILON);
}
END_TEST


START_TEST(test_min)
{
  struct mol_vector3 x = {1.0, 2.0, 3.0};
  struct mol_vector3 y = {0.0, 3.0, 4.0};
  struct mol_vector3 dst;

  MOL_VEC_MIN(dst, x, y);

  ck_assert(fabs(dst.X - 0.0) <= DBL_EPSILON);
  ck_assert(fabs(dst.Y - 2.0) <= DBL_EPSILON);
  ck_assert(fabs(dst.Z - 3.0) <= DBL_EPSILON);

  MOL_VEC_MIN(dst, y, x);

  ck_assert(fabs(dst.X - 0.0) <= DBL_EPSILON);
  ck_assert(fabs(dst.Y - 2.0) <= DBL_EPSILON);
  ck_assert(fabs(dst.Z - 3.0) <= DBL_EPSILON);

  MOL_VEC_MIN(dst, y, y);

  ck_assert(fabs(dst.X - 0.0) <= DBL_EPSILON);
  ck_assert(fabs(dst.Y - 3.0) <= DBL_EPSILON);
  ck_assert(fabs(dst.Z - 4.0) <= DBL_EPSILON);
}
END_TEST


START_TEST(test_max)
{
  struct mol_vector3 x = {1.0, 2.0, 3.0};
  struct mol_vector3 y = {0.0, 3.0, 4.0};
  struct mol_vector3 dst;

  MOL_VEC_MAX(dst, x, y);

  ck_assert(fabs(dst.X - 1.0) <= DBL_EPSILON);
  ck_assert(fabs(dst.Y - 3.0) <= DBL_EPSILON);
  ck_assert(fabs(dst.Z - 4.0) <= DBL_EPSILON);

  MOL_VEC_MAX(dst, y, x);

  ck_assert(fabs(dst.X - 1.0) <= DBL_EPSILON);
  ck_assert(fabs(dst.Y - 3.0) <= DBL_EPSILON);
  ck_assert(fabs(dst.Z - 4.0) <= DBL_EPSILON);

  MOL_VEC_MAX(dst, y, y);

  ck_assert(fabs(dst.X - 0.0) <= DBL_EPSILON);
  ck_assert(fabs(dst.Y - 3.0) <= DBL_EPSILON);
  ck_assert(fabs(dst.Z - 4.0) <= DBL_EPSILON);
}
END_TEST

START_TEST(test_copy)
{
  struct mol_vector3u x = {1u, 2u, 3u};
  struct mol_vector3 dst;

  MOL_VEC_COPY(dst, x);

  ck_assert(fabs(dst.X - 1.0) <= DBL_EPSILON);
  ck_assert(fabs(dst.Y - 2.0) <= DBL_EPSILON);
  ck_assert(fabs(dst.Z - 3.0) <= DBL_EPSILON);
}
END_TEST

START_TEST(test_eq)
{
  struct mol_vector3i x = {1, 2, 3};

  ck_assert(MOL_VEC_EQ(x, x));
  ck_assert(!MOL_VEC_NEQ(x, x));

  struct mol_vector3i y1 = {2, 2, 3};
  ck_assert(!MOL_VEC_EQ(x, y1));
  ck_assert(MOL_VEC_NEQ(x, y1));

  struct mol_vector3i y2 = {1, 3, 3};
  ck_assert(!MOL_VEC_EQ(x, y2));
  ck_assert(MOL_VEC_NEQ(x, y2));

  struct mol_vector3i y3 = {1, 2, 1};
  ck_assert(!MOL_VEC_EQ(x, y3));
  ck_assert(MOL_VEC_NEQ(x, y3));

}
END_TEST


Suite *vector_suite(void)
{
  Suite *suite = suite_create("vector_math");

  // Math test cases
  TCase *tcase_math = tcase_create("test_math");
  tcase_add_test(tcase_math, test_add);
  tcase_add_test(tcase_math, test_subtract);
  tcase_add_test(tcase_math, test_multiply);
  tcase_add_test(tcase_math, test_divide);
  tcase_add_test(tcase_math, test_add_scalar);
  tcase_add_test(tcase_math, test_subtract_scalar);
  tcase_add_test(tcase_math, test_multiply_scalar);
  tcase_add_test(tcase_math, test_divide_scalar);
  tcase_add_test(tcase_math, test_dot_prod);
  tcase_add_test(tcase_math, test_cross_prod);
  tcase_add_test(tcase_math, test_min);
  tcase_add_test(tcase_math, test_max);
  tcase_add_test(tcase_math, test_copy);
  tcase_add_test(tcase_math, test_eq);

  suite_add_tcase(suite, tcase_math);

  return suite;
}

int main(void)
{
  Suite *suite = vector_suite();
  SRunner *runner = srunner_create(suite);
  srunner_run_all(runner, CK_ENV);

  int number_failed = srunner_ntests_failed(runner);
  srunner_free(runner);
  return number_failed;
}
