#include <stdio.h>
#include <check.h>
#include <errno.h>
#include <float.h>
#include <math.h>

#include "matrix.h"

// Test cases
START_TEST(test_read)
{
  struct mol_matrix3_list *rots = mol_matrix3_list_from_file("rot.prm");

  ck_assert(rots != NULL);

  ck_assert_int_eq(rots->size, 8);

  ck_assert(fabs(rots->members[2].m11 - 0.11111111111111111111) <= DBL_EPSILON);
  ck_assert(fabs(rots->members[2].m12 - 0.22222222222222222222) <= DBL_EPSILON);
  ck_assert(fabs(rots->members[2].m13 - 0.33333333333333333333) <= DBL_EPSILON);
  ck_assert(fabs(rots->members[2].m21 - 0.44444444444444444444) <= DBL_EPSILON);
  ck_assert(fabs(rots->members[2].m22 - 0.55555555555555555555) <= DBL_EPSILON);
  ck_assert(fabs(rots->members[2].m23 - 0.66666666666666666666) <= DBL_EPSILON);
  ck_assert(fabs(rots->members[2].m31 - 0.77777777777777777777) <= DBL_EPSILON);
  ck_assert(fabs(rots->members[2].m32 - 0.88888888888888888888) <= DBL_EPSILON);
  ck_assert(fabs(rots->members[2].m33 - 0.99999999999999999999) <= DBL_EPSILON);

  mol_list_free(rots);
}
END_TEST

START_TEST(test_readf)
{
  struct mol_matrix3f_list *rots = mol_matrix3f_list_from_file("rot.prm");

  ck_assert(rots != NULL);

  ck_assert_int_eq(rots->size, 8);

  ck_assert(fabsf(rots->members[2].m11 - 0.11111111111111111111f) <= FLT_EPSILON);
  ck_assert(fabsf(rots->members[2].m12 - 0.22222222222222222222f) <= FLT_EPSILON);
  ck_assert(fabsf(rots->members[2].m13 - 0.33333333333333333333f) <= FLT_EPSILON);
  ck_assert(fabsf(rots->members[2].m21 - 0.44444444444444444444f) <= FLT_EPSILON);
  ck_assert(fabsf(rots->members[2].m22 - 0.55555555555555555555f) <= FLT_EPSILON);
  ck_assert(fabsf(rots->members[2].m23 - 0.66666666666666666666f) <= FLT_EPSILON);
  ck_assert(fabsf(rots->members[2].m31 - 0.77777777777777777777f) <= FLT_EPSILON);
  ck_assert(fabsf(rots->members[2].m32 - 0.88888888888888888888f) <= FLT_EPSILON);
  ck_assert(fabsf(rots->members[2].m33 - 0.99999999999999999999f) <= FLT_EPSILON);
  mol_list_free(rots);
}
END_TEST

START_TEST(test_readl)
{
  struct mol_matrix3l_list *rots = mol_matrix3l_list_from_file("rot.prm");

  ck_assert(rots != NULL);

  ck_assert_int_eq(rots->size, 8);

  ck_assert(fabsl(rots->members[2].m11 - 0.11111111111111111111l) <= LDBL_EPSILON);
  ck_assert(fabsl(rots->members[2].m12 - 0.22222222222222222222l) <= LDBL_EPSILON);
  ck_assert(fabsl(rots->members[2].m13 - 0.33333333333333333333l) <= LDBL_EPSILON);
  ck_assert(fabsl(rots->members[2].m21 - 0.44444444444444444444l) <= LDBL_EPSILON);
  ck_assert(fabsl(rots->members[2].m22 - 0.55555555555555555555l) <= LDBL_EPSILON);
  ck_assert(fabsl(rots->members[2].m23 - 0.66666666666666666666l) <= LDBL_EPSILON);
  ck_assert(fabsl(rots->members[2].m31 - 0.77777777777777777777l) <= LDBL_EPSILON);
  ck_assert(fabsl(rots->members[2].m32 - 0.88888888888888888888l) <= LDBL_EPSILON);
  ck_assert(fabsl(rots->members[2].m33 - 0.99999999999999999999l) <= LDBL_EPSILON);
  mol_list_free(rots);
}
END_TEST

Suite *rotations_suite(void)
{
	Suite *suite = suite_create("rotations");

	TCase *tcase = tcase_create("test");
	tcase_add_test(tcase, test_read);
	tcase_add_test(tcase, test_readf);
	tcase_add_test(tcase, test_readl);

	suite_add_tcase(suite, tcase);

	return suite;
}

int main(void)
{
	Suite *suite = rotations_suite();
	SRunner *runner = srunner_create(suite);
	srunner_run_all(runner, CK_ENV);

	int number_failed = srunner_ntests_failed(runner);
	srunner_free(runner);
	return number_failed;
}
