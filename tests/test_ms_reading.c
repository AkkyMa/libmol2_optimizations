#include <stdio.h>
#include <check.h>
#include <errno.h>

#include "atom_group.h"
#include "ms.h"

START_TEST(test_read_single)
{
	struct mol_atom_group *ag = mol_read_ms("test.ms");
	ck_assert(ag != NULL);
	ck_assert_int_eq(ag->natoms, 4021);
	//ATOM      2  CA  GLU H   0      -9.966  14.057  59.651   9.9
	ck_assert_str_eq(ag->atom_name[1], " CA ");
	ck_assert(ag->alternate_location[1] == ' ');
	ck_assert_str_eq(ag->residue_name[1], "GLU");
	ck_assert(ag->residue_id[1].chain == 'H');
	ck_assert_int_eq(ag->residue_id[1].residue_seq, 0);
	ck_assert(ag->residue_id[1].insertion == ' ');
	ck_assert(ag->coords[1].X == -9.966);
	ck_assert(ag->coords[1].Y == 14.057);
	ck_assert(ag->coords[1].Z == 59.651);
	ck_assert(ag->attraction_level[1] == 9.9);
	ck_assert(ag->surface[1] == true);

	ck_assert(ag->attraction_level[2] == 0.0);
	ck_assert(ag->surface[2] == false);

	for (size_t i = 0; i < ag->natoms; ++i) {
		struct mol_residue *res = find_residue(ag,
						ag->residue_id[i].chain,
						ag->residue_id[i].residue_seq,
						ag->residue_id[i].insertion);
		ck_assert(res != NULL);
		ck_assert(i >= res->atom_start);
		ck_assert(i <= res->atom_end);
	}

	for (size_t i = 0; i < ag->nresidues - 1; i++) {
		struct mol_residue *a = ag->residue_list[i];
		struct mol_residue *b = ag->residue_list[i+1];

		ck_assert(a != NULL);
		ck_assert(b != NULL);

		ck_assert(mol_residue_id_cmp(&a->residue_id,
					     &b->residue_id) == -1);
	}

	mol_atom_group_free(ag);
}
END_TEST

START_TEST(test_read_nonexistent)
{
	struct mol_atom_group *ag = mol_read_ms("nonexistent");
	ck_assert(ag == NULL);
}
END_TEST

START_TEST(test_read_no_atoms)
{
	struct mol_atom_group *ag = mol_read_ms("no_atoms.ms");
	ck_assert(ag == NULL);
}
END_TEST

Suite *atom_group_suite(void)
{
	Suite *suite = suite_create("ms_reading");

	TCase *tcase_read = tcase_create("test_read");
	tcase_add_test(tcase_read, test_read_single);

	TCase *tcase_errors = tcase_create("test_read_errors");
	tcase_add_test(tcase_errors, test_read_nonexistent);
	tcase_add_test(tcase_errors, test_read_no_atoms);

	suite_add_tcase(suite, tcase_read);
	suite_add_tcase(suite, tcase_errors);

	return suite;
}

int main(void)
{
	Suite *suite = atom_group_suite();
	SRunner *runner = srunner_create(suite);
	srunner_run_all(runner, CK_ENV);

	int number_failed = srunner_ntests_failed(runner);
	srunner_free(runner);
	return number_failed;
}
