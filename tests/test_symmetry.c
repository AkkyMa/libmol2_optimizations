#include <stdio.h>
#include <check.h>
#include <math.h>

#include "atom_group.h"
#include "pdb.h"
#include "icharmm.h"
#include "rmsd.h"

// Test cases
START_TEST(test_symmetry1)
{
	struct mol_atom_group *ag = mol_read_pdb("phenol.pdb");
	mol_atom_group_read_geometry(ag, "phenol.psf", "small.prm", "small.rtf");
	struct mol_symmetry_list *symmetry =  mol_detect_symmetry(ag);

	ck_assert_int_eq(symmetry->size, 2);

	//first symmetry set should be identity
	for (size_t i = 0; i < ag->natoms; i++) {
		ck_assert_int_eq(symmetry->members[i], i);
	}

	//other symmetry should be flip of rint
	ck_assert_int_eq(symmetry->members[ag->natoms], 0);
	ck_assert_int_eq(symmetry->members[ag->natoms+1], 5);
	ck_assert_int_eq(symmetry->members[ag->natoms+2], 4);
	ck_assert_int_eq(symmetry->members[ag->natoms+3], 3);
	ck_assert_int_eq(symmetry->members[ag->natoms+4], 2);
	ck_assert_int_eq(symmetry->members[ag->natoms+5], 1);
	ck_assert_int_eq(symmetry->members[ag->natoms+6], 6);
	ck_assert_int_eq(symmetry->members[ag->natoms+7], 7);

	mol_list_free(symmetry);
	mol_atom_group_free(ag);
}
END_TEST

START_TEST(test_symmetry2)
{
	struct mol_atom_group *ag = mol_read_pdb("butane.pdb");
	mol_atom_group_read_geometry(ag, "butane.psf", "small.prm", "small.rtf");
	struct mol_symmetry_list *symmetry =  mol_detect_symmetry(ag);

	ck_assert_int_eq(symmetry->size, 2);

	//first symmetry set should be identity
	for (size_t i = 0; i < ag->natoms; i++) {
		ck_assert_int_eq(symmetry->members[i], i);
	}

	//other symmetry should be flip of rint
	ck_assert_int_eq(symmetry->members[ag->natoms], 3);
	ck_assert_int_eq(symmetry->members[ag->natoms+1], 2);
	ck_assert_int_eq(symmetry->members[ag->natoms+2], 1);
	ck_assert_int_eq(symmetry->members[ag->natoms+3], 0);
	mol_list_free(symmetry);
	mol_atom_group_free(ag);
}
END_TEST

Suite *symmetry_suite(void)
{
	Suite *suite = suite_create("symmetry");

	// Add test cases here
	// Each test case can call multiple test functions
	// Too add a test case, call tcase_add_test
	// The first argument is the TCase struct, the second is the
	//  test function name.
	TCase *tcase = tcase_create("test");
	tcase_set_timeout(tcase, 10);
	tcase_add_test(tcase, test_symmetry1);
	tcase_add_test(tcase, test_symmetry2);

	suite_add_tcase(suite, tcase);

	return suite;
}

int main(void)
{
	Suite *suite = symmetry_suite();
	SRunner *runner = srunner_create(suite);
	srunner_run_all(runner, CK_ENV);

	int number_failed = srunner_ntests_failed(runner);
	srunner_free(runner);
	return number_failed;
}

