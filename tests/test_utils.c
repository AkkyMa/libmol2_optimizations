#include <ctype.h>
#include <string.h>
#include <check.h>
#include <stdio.h>
#include <math.h>
#include <signal.h>
#include "utils.h"


START_TEST(test_sizecmp)
{
	size_t smaller = 5;
	size_t bigger = 6;

	ck_assert(size_t_cmp(&bigger, &smaller) == 1);
	ck_assert(size_t_cmp(&smaller, &bigger) == -1);

	size_t dupl = 6;

	ck_assert(size_t_cmp(&bigger, &dupl) == 0);
}
END_TEST


START_TEST(test_rstrip)
{
	char spacey_string[] = "8spacesafter   ";

	rstrip(spacey_string);

	ck_assert(strlen(spacey_string) == 12);
	ck_assert(spacey_string[strlen(spacey_string)] == '\0');
	ck_assert(strcmp(spacey_string, "8spacesafter") == 0);

	char messy_spaces[] = "thing s	";

	rstrip(messy_spaces);

	ck_assert(strlen(messy_spaces) == 7);
	ck_assert(messy_spaces[strlen(messy_spaces)] == '\0');
	ck_assert(strcmp(spacey_string, "8spacesafter") == 0);
}
END_TEST


static float _divide_by_zero(float a)
{
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdiv-by-zero"
	a = a / 0;
#pragma GCC diagnostic pop
	ck_assert(a == INFINITY);
	return a;
}

START_TEST(test_floating_point_exceptions_disabled_by_default)
{
	// Check that floating-point math works properly
	_divide_by_zero(1);
}
END_TEST


START_TEST(test_enable_floating_point_exceptions)
{
	// Now try dividing with SIGFPE enabled
	mol_enable_floating_point_exceptions();
	_divide_by_zero(1);
}
END_TEST


#ifdef __SANITIZE_ADDRESS__
// For this test set, we don't want to let ASAN handle floating point exceptions (FPEs).
// In fact, we probably don't want it to do it anywhere, since Check takes care of all signals.
// But particularly, we want to raise FPE in test_enable_floating_point_exceptions, and we don't want ASAN to interfere.
const char *__asan_default_options(void) {
	return "handle_sigfpe=0";
}
#endif


Suite* utils_test_suite(void)
{
	Suite* suite = suite_create("utils");

	TCase* tcases = tcase_create("test_utils_lib");
	tcase_add_test(tcases, test_sizecmp);
	tcase_add_test(tcases, test_rstrip);

	// By default, floating point exceptions (FPEs) should not be raised. Check it.
	tcase_add_test(tcases, test_floating_point_exceptions_disabled_by_default);

#if (!defined(__APPLE__) && !defined(_WIN32))
	// For Linux, check that the FPEs occur when enabled.
	/* The flags we use here are different from utils.c. This is intentional.
	 * In utils.c, we want to safeguard as much as possible, so the library compiles reliably.
	 * Here, we want to test the desired behavior: the floating point exceptions are raised on Linux.
	 * So, if you're using some unusual build setup (e.g., custom libc on Linux), this test might fail. */
	tcase_add_test_raise_signal(tcases, test_enable_floating_point_exceptions, SIGFPE);
#else
	// For Windows and MacOS, check that trying to enable FPEs them don't break anything (although FPE is not raised).
	tcase_add_test(tcases, test_enable_floating_point_exceptions);
#endif

	suite_add_tcase(suite, tcases);

	return suite;
}

int main(void)
{
	Suite* suite = utils_test_suite();
	SRunner* runner = srunner_create(suite);
	srunner_run_all(runner, CK_ENV);

	int number_failed = srunner_ntests_failed(runner);
	srunner_free(runner);
	return number_failed;
}
