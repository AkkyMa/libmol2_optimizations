! - parameter file PARAM19 -
! PEPTIDE GEOMETRY FROM RAMACHANDRAN ET AL BBA 359:298 (1974)
! TORSIONS FROM HAGLER ET AL JACS 98:4600 (1976)
! JORGENSEN NONBOND PARAMETERS JACS 103:3976-3985 WITH 1-4 RC=1.80/0.1
!
!* - PEPTIDE GEOMETRY TO GIVE RAMACHANDRAN ET AL BBA 359:298 (1974)
!* - PEPTIDE TORSIONS FROM HAGLER ET AL JACS 98:4600 (1976)
!* - NONBONDED TERMS JORGENSEN JACS 103:3976 W/ RC1-4 = 1.80 EC1-4 = 0.1
!*  The default h-bond exponents are now 6-repul 4-attr
!* ++++++++ ATOMTYPE OS (IN METHYL ESTER) ADDED FOR CHARMM COURSE /LN ++++
!* SOLVENT PARAMETERS: SUPPORTING ST2 AND MODIFIED TIP3P MODEL
!* Switched from Slater-Kirkwood to simple mixing rules - AB
!* Hbond parameters based on comparisons of dimer results with
!*   ab initio calculations. - WER  12/19/84
!* Grouping of atom types for VDW parameters - BRB 1/3/85
!*
!references
!Reiher, III., W.E. Theoretical Studies of Hydrogen Bonding, Ph.D.
!Thesis, Department of Chemistry, Harvard University, Cambridge, MA,
!USA, 1985
!
!and
!
!Neria, E., Fischer, S., and Karplus, M.  Simulation of Activation Free
!Energies in Molecular Systems, Journal of Chemical Physics, 1996, 105:
!1902-21.

BONDS
C    C      450.0       1.38!  FROM B. R. GELIN THESIS AMIDE AND DIPEPTIDES
C    CH1E   405.0       1.52!  EXCEPT WHERE NOTED.  CH1E,CH2E,CH3E, AND CT
C    CH3E   405.0       1.52
C    NH1    471.0       1.33
C    O      580.0       1.23
C    OC     580.0       1.23!  FORCE DECREASE AND LENGTH INCREASE FROM C O
CH1E CH1E   225.0       1.53
CH1E CH3E   225.0       1.52
CH1E NH1    422.0       1.45
CH1E NH3    422.0       1.45
CH3E NH1    422.0       1.49
H    NH1    405.0       0.98!  GELIN AND IR STRETCH 3200 CM 1
H    H        0.0       1.6329931 ! from ST2 geometry (for SHAKE w/PARAM)
HC   NH1    405.0       0.98
HC   NH3    405.0       1.04

ANGLES
C    C    C       70.0     106.5!  FROM B. R. GELIN THESIS WITH HARMONIC
C    C    CH3E    65.0     126.5!  WITH EXTENDED H COMPENSATED FOR LACK
C    C    NH1     65.0     109.0!  TO N-METHYL ACETAMIDE VIBRATIONS.
C    C    O       65.0     119.0 ! FOR NETROPSIN
CH1E C    NH1     20.0     117.5
CH1E C    O       85.0     121.5
CH1E C    OC      85.0     117.5
CH3E C    NH1     20.0     117.5
CH3E C    O       85.0     121.5
NH1  C    O       65.0     121.0
OC   C    OC      85.0     122.5
C    CH1E CH1E    70.0     110.0
C    CH1E CH3E    70.0     106.5
C    CH1E NH1     45.0     111.6
C    CH1E NH3     45.0     111.6
CH1E CH1E CH3E    45.0     111.0
CH1E CH1E NH1     50.0     110.0
CH1E CH1E NH3     50.0     107.5
CH3E CH1E CH3E    50.0     111.0
CH3E CH1E NH1     65.0     108.5
CH3E CH1E NH3     65.0     109.5
C    NH1  C       60.0     102.5  ! UNUSED (AND PROBABLY WRONG)
C    NH1  CH1E    77.5     120.0
C    NH1  CH3E    77.5     120.0
C    NH1  H       30.0     120.0
CH1E NH1  CH3E    60.0     120.0
CH1E NH1  H       35.0     120.0
CH3E NH1  H       35.0     120.0
CH1E NH3  HC      35.0     109.5
HC   NH3  HC      40.0     109.5

DIHEDRALS
X    C    CH1E X        0.0       3       0.0! FROM GELIN THESIS AMIDES
X    C    NH1  X        8.2       2     180.0! THE EXPERIMENTAL BARRIER.
X    CH1E CH1E X        1.6       3       0.0
X    CH1E NH1  X        0.3       3       0.0! EXP. DATA AND 6 31G CALC.
X    CH1E NH3  X        0.6       3       0.0! 1/PROTON SO 3 FOR THE BOND

IMPROPER
C    OC   OC   CH1E   100.0    0     0.0! CARBOXYL OUT OF PLANE.
C    X    X    C       25.0    0     0.0! FROM BENZENE NORMAL MODE ANALYSIS
C    X    X    CH3E    90.0    0     0.0
C    X    X    H       75.0    0     0.0! FROM BENZENE NORMAL MODE ANALYSIS
C    X    X    NH1    100.0    0     0.0! AMIDES FIT TO N METHYL ACETAMIDE.
C    X    X    O      100.0    0     0.0
C    X    X    OC     100.0    0     0.0
CH1E X    X    CH1E    55.0    0   35.26439! CALCULATED TO  BE THE SAME AS THE 3
CH1E X    X    CH3E    55.0    0   35.26439
H    X    X    O       45.0    0     0.0
NH1  X    X    CH1E    45.0    0     0.0
NH1  X    X    CH3E    45.0    0     0.0
NH1  X    X    H       45.0    0     0.0! AMIDES PROTON OOP
NH1  X    X    NH1     25.0    0     0.0!

NONBONDED  NBXMOD 5  ATOM CDIEL SHIFT VATOM VDISTANCE VSHIFT -
     CUTNB 8.0  CTOFNB 7.5  CTONNB 6.5  EPS 1.0  E14FAC 0.4  WMIN 1.5
H        0.0440    -0.0498    0.8000 
HC       0.0440    -0.0498    0.6000 ! charged group. Reduced vdw radius
C        1.65    -0.1200       2.100 1.65 -0.1 1.9 ! carbonyl carbon
CH1E     1.35    -0.0486       2.365 1.35 -0.1 1.9 ! \
CH3E     2.17    -0.1811       2.165 1.77 -0.1 1.9 ! /
OC       2.1400    -0.6469    1.6000  


HBOND AEXP 4 REXP 6 HAEX 0 AAEX 0   NOACCEPTORS  HBNOEXCLUSIONS  ALL  -
   CUTHB 0.5 CTOFHB 5.0 CTONHB 4.0  CUTHA 90.0  CTOFHA 90.0  CTONHA 90.0
!
END
