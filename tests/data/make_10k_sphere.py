#!/usr/bin/env python3

import random
import math

radius = 100.0

for i in range(5):

    # we do 1999 random placements,
    # then place one on the surface
    for j in range(1999):
        total = float("inf")
        while total >= 1.0:
            x = 2.0*(random.random() - 0.5)
            y = 2.0*(random.random() - 0.5)
            z = 2.0*(random.random() - 0.5)

            total = x*x + y*y + z*z

        x *= radius
        y *= radius
        z *= radius

        atom_i = i * 2000 + j + 1

        print("ATOM  %5d  C   DUM     1    %8.3f%8.3f%8.3f" % (atom_i, x, y, z))

    x = 2.0*(random.random() - 0.5)
    y = 2.0*(random.random() - 0.5)
    z = 2.0*(random.random() - 0.5)

    total = x*x + y*y + z*z
    total = math.sqrt(total)

    x /= total
    y /= total
    z /= total

    x *= radius
    y *= radius
    z *= radius

    atom_i = (i+1) * 2000

    print("ATOM  %5d  C   DUM     1    %8.3f%8.3f%8.3f" % (atom_i, x, y, z))
