#include <stdio.h>
#include <check.h>
#include <errno.h>

#include "mask.h"
#include "pdb.h"

// Test cases
START_TEST(test_mask_all)
{
	struct mol_atom_group *target = mol_read_pdb("1rei.pdb");
	struct mol_atom_group *mask = mol_read_pdb("1rei.pdb");

	ck_assert(target != NULL);
	ck_assert(mask != NULL);

	mol_atom_group_mask(target, mask, 1.0);

	for (size_t i = 0; i < target->natoms; i++) {
		ck_assert(target->mask[i]);
	}
	mol_atom_group_free(target);
	mol_atom_group_free(mask);
}
END_TEST

START_TEST(test_mask_part)
{
	struct mol_atom_group *target = mol_read_pdb("phenol.pdb");
	struct mol_atom_group *mask = mol_read_pdb("butane.pdb");

	ck_assert(target != NULL);
	ck_assert(mask != NULL);

	mol_atom_group_mask(target, mask, 1.0);

	const bool expected[8] = {false, false, false, true, false, false, true, true};

	for (size_t i = 0; i < target->natoms; i++) {
		ck_assert(target->mask[i] == expected[i]);
	}
	mol_atom_group_free(target);
	mol_atom_group_free(mask);
}
END_TEST


START_TEST(test_null)
{
	errno = 0;
	mol_atom_group_mask(NULL, NULL, 1.0);
	ck_assert(errno == EFAULT);
}
END_TEST


Suite *mask_suite(void)
{
	Suite *suite = suite_create("mask");

	// Add test cases here
	// Each test case can call multiple test functions
	// Too add a test case, call tcase_add_test
	// The first argument is the TCase struct, the second is the
	//  test function name.
	TCase *tcase = tcase_create("test");
	tcase_add_test(tcase, test_mask_all);
	tcase_add_test(tcase, test_mask_part);
	tcase_add_test(tcase, test_null);

	suite_add_tcase(suite, tcase);

	return suite;
}

int main(void)
{
	Suite *suite = mask_suite();
	SRunner *runner = srunner_create(suite);
	srunner_run_all(runner, CK_ENV);

	int number_failed = srunner_ntests_failed(runner);
	srunner_free(runner);
	return number_failed;
}
