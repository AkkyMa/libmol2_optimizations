#include <stdio.h>
#include <check.h>
#include <errno.h>

#include "alignment.h"
#include "atom_group.h"
#include "pdb.h"

START_TEST(test_align_seqres)
{
	/* ala_gapped.pdb is a very special * poly-A. It ensures that an alignment
	 * puts the gaps in the right place, even when the sequences * match multiple
	 * ways.
	 *
	 * Two chains of:
	 * AA gap AA gap AA
	 *
	 * Since the sequence is all As, you won't expect to get * the pattern by
	 * chance
	 */
	struct mol_atom_group *ag = mol_read_pdb("ala_gapped.pdb");
	ck_assert(ag != NULL);

	struct mol_alignment *aln = mol_align_to_seqres(ag);

	ck_assert(strcmp(aln->seq1, "AAAAAAAA/AAAAAAAA") == 0);
	ck_assert(strcmp(aln->seq2, "AA-AA-AA/AA-AA-AA") == 0);
	mol_atom_group_free(ag);
	free_mol_alignment(aln);
}
END_TEST

Suite *align_seqres_suite(void)
{
	Suite *suite = suite_create("align_seqres");

	TCase *tcases = tcase_create("test_align_seqres");
	tcase_add_test(tcases, test_align_seqres);

	suite_add_tcase(suite, tcases);

	return suite;
}

int main(void)
{
	Suite *suite = align_seqres_suite();
	SRunner *runner = srunner_create(suite);
	srunner_run_all(runner, CK_ENV);

	int number_failed = srunner_ntests_failed(runner);
	srunner_free(runner);
	return number_failed;
}
