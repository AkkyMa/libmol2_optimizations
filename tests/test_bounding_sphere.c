#include <stdio.h>
#include <check.h>
#include <errno.h>
#include <float.h>
#include <math.h>

#include "atom_group.h"
#include "pdb.h"
#include "vector.h"

START_TEST(test_small_spheres)
{
	struct mol_atom_group *ag = mol_read_pdb("sphere1.pdb");
	struct mol_vector3 center;

	/* sphere1 and sphere2 have a point outside the two furthest away points
	 * with three points, we could circumscribe the triangle as having
	 * a center at (0.5, 0.39/1.6, 0.0) with diameter 1.1125
	 * */
	double diameter = mol_atom_group_bounding_sphere(&center, ag);
	printf("new: %f\n", diameter);
	ck_assert(diameter > (1.1125-DBL_EPSILON));
	ck_assert(diameter < 1.117); //do not want it to be too big
	for (size_t i = 0; i < ag->natoms; i++) {
		double dist = MOL_VEC_EUCLIDEAN_DIST(center, ag->coords[i]);
		ck_assert(dist < diameter);
	}
	mol_atom_group_free(ag);

	ag = mol_read_pdb("sphere2.pdb");
	diameter = mol_atom_group_bounding_sphere(&center, ag);
	printf("new: %f\n", diameter);
	ck_assert(diameter > (1.1125-DBL_EPSILON));
	ck_assert(diameter < 1.117); //do not want it to be too big
	for (size_t i = 0; i < ag->natoms; i++) {
		double dist = MOL_VEC_EUCLIDEAN_DIST(center, ag->coords[i]);
		ck_assert(dist < diameter);
	}
	mol_atom_group_free(ag);

	/* sphere3 and sphere4 have a point inside the two furthest away points
	 * We expect to find the perfect diameter of 1.0 and
	 * a center at (0.5, 0.0, 0.0)
	 * */
	ag = mol_read_pdb("sphere3.pdb");
	diameter = mol_atom_group_bounding_sphere(&center, ag);
	printf("new: %f\n", diameter);
	ck_assert(fabs(diameter - 1.0) <= DBL_EPSILON);
	ck_assert(fabs(center.X - 0.5) <= DBL_EPSILON);
	ck_assert(fabs(center.Y) <= DBL_EPSILON);
	ck_assert(fabs(center.Z) <= DBL_EPSILON);
	mol_atom_group_free(ag);

	ag = mol_read_pdb("sphere4.pdb");
	diameter = mol_atom_group_bounding_sphere(&center, ag);
	printf("new: %f\n", diameter);
	ck_assert(fabs(diameter - 1.0) <= DBL_EPSILON);
	ck_assert(fabs(center.X - 0.5) <= DBL_EPSILON);
	ck_assert(fabs(center.Y) <= DBL_EPSILON);
	ck_assert(fabs(center.Z) <= DBL_EPSILON);
	mol_atom_group_free(ag);
}
END_TEST

START_TEST(test_10k_sphere)
{
	struct mol_atom_group *ag = mol_read_pdb("10k_sphere.pdb");
	struct mol_vector3 center;

	/* 10k_sphere is 10k points with a radius 100. 5 points are put
	 * at radius 100.
	 * */
	double diameter = mol_atom_group_bounding_sphere(&center, ag);
	printf("new: %f\n", diameter);
	ck_assert(diameter > (200.0-DBL_EPSILON));
	ck_assert(diameter < 202.0); //do not want it to be too big
	for (size_t i = 0; i < ag->natoms; i++) {
		double dist = MOL_VEC_EUCLIDEAN_DIST(center, ag->coords[i]);
		ck_assert(dist < diameter);
	}
	mol_atom_group_free(ag);
}
END_TEST

Suite *sphere_suite(void)
{
	Suite *suite = suite_create("bounding_sphere");

	TCase *tcases = tcase_create("test_bounding_sphere");
	tcase_add_test(tcases, test_small_spheres);
	tcase_add_test(tcases, test_10k_sphere);

	suite_add_tcase(suite, tcases);

	return suite;
}

int main(void)
{
	Suite *suite = sphere_suite();
	SRunner *runner = srunner_create(suite);
	srunner_run_all(runner, CK_ENV);

	int number_failed = srunner_ntests_failed(runner);
	srunner_free(runner);
	return number_failed;
}
