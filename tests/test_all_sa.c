#include <stdio.h>
#include <check.h>
#include <errno.h>

#include "sasa.h"
#include "pdb.h"

// Test cases
START_TEST(test_all_sa)
{
	struct mol_atom_group *target = mol_read_pdb("1rei.pdb");

	ck_assert(target != NULL);
	mol_mark_all_sa(target);
	ck_assert(target->surface != NULL);

	for (size_t i = 0; i < target->natoms; i++) {
		ck_assert(target->surface[i]);
	}
	mol_atom_group_free(target);
}
END_TEST

Suite *all_sa_suite(void)
{
	Suite *suite = suite_create("all_sa");

	// Add test cases here
	// Each test case can call multiple test functions
	// Too add a test case, call tcase_add_test
	// The first argument is the TCase struct, the second is the
	//  test function name.
	TCase *tcase = tcase_create("test");
	tcase_add_test(tcase, test_all_sa);

	suite_add_tcase(suite, tcase);

	return suite;
}

int main(void)
{
	Suite *suite = all_sa_suite();
	SRunner *runner = srunner_create(suite);
	srunner_run_all(runner, CK_ENV);

	int number_failed = srunner_ntests_failed(runner);
	srunner_free(runner);
	return number_failed;
}
