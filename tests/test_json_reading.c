#include <stdio.h>
#include <check.h>
#include <errno.h>

#include "atom_group.h"
#include "json.h"

START_TEST(test_read_json)
{
	struct mol_atom_group *ag = mol_read_json("c1m.atlasprobe");
	ck_assert(ag != NULL);
	ck_assert_int_eq(ag->natoms, 12);
	ck_assert_int_eq(ag->nbonds, 13);
	ck_assert_int_eq(ag->nangles, 17);
	ck_assert_int_eq(ag->ndihedrals, 21);
	ck_assert_int_eq(ag->nimpropers, 3);

	ck_assert_int_eq(ag->bonds[0].ai, 0);
	ck_assert_int_eq(ag->bonds[0].aj, 1);

	ck_assert_int_eq(ag->residue_id[0].residue_seq, 1);
	ck_assert(ag->residue_id[0].insertion == ' ');
	ck_assert(ag->record[0] == MOL_HETATM);
	ck_assert(ag->record[1] == MOL_ATOM);

	ck_assert(ag->hbond_prop[8] == 0);
	ck_assert(ag->hbond_prop[9] == HBOND_ACCEPTOR);

	mol_atom_group_free(ag);
}
END_TEST

START_TEST(test_read_nonexistent)
{
	struct mol_atom_group *ag = mol_read_json("nonexistent");
	ck_assert(ag == NULL);
}
END_TEST

Suite *json_suite(void)
{
	Suite *suite = suite_create("json_reading");

	TCase *tcase_read = tcase_create("test_read");
	tcase_add_test(tcase_read, test_read_json);

	TCase *tcase_errors = tcase_create("test_read_errors");
	tcase_add_test(tcase_errors, test_read_nonexistent);

	suite_add_tcase(suite, tcase_read);
	suite_add_tcase(suite, tcase_errors);

	return suite;
}

int main(void)
{
	Suite *suite = json_suite();
	SRunner *runner = srunner_create(suite);
	srunner_run_all(runner, CK_ENV);

	int number_failed = srunner_ntests_failed(runner);
	srunner_free(runner);
	return number_failed;
}
