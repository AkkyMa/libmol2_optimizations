#include <stdio.h>
#include <check.h>
#include <errno.h>
#include <string.h>

#include "atom_group.h"
#include "json.h"
#include "pdb.h"
#include "pdbqt.h"


START_TEST(test_read_small_molecule)
{
	// File produced by AutoDock
	struct mol_atom_group *ag = mol_pdbqt_read("cats.pdbqt");
	ck_assert(ag != NULL);
	ck_assert_int_eq(ag->natoms, 46);

	ck_assert_str_eq(ag->atom_name[9], " C15");
	ck_assert_str_eq(ag->element[9], "C");
	ck_assert(ag->coords[9].X == -3.058);
	ck_assert(ag->coords[9].Y == 11.560);
	ck_assert(ag->coords[9].Z == -8.826);
	ck_assert(ag->occupancy[9] == 1.00);
	ck_assert(ag->B[9] == 0.00);
	ck_assert(ag->charge[9] == 0.158);
	ck_assert(mol_atom_group_has_metadata(ag, MOL_PDBQT_ATOM_TYPE_METADATA_KEY));
	const enum mol_autodock_atom_type *autodock_type = \
		(const enum mol_autodock_atom_type *) \
			mol_atom_group_fetch_metadata(ag, MOL_PDBQT_ATOM_TYPE_METADATA_KEY);
	ck_assert_int_eq(autodock_type[9], AUTODOCK_ATOM_C);

	for (size_t i = 0; i < ag->natoms; ++i) {
		ck_assert(ag->alternate_location[i] == ' ');
		ck_assert_str_eq(ag->residue_name[i], "X00");
		ck_assert(ag->residue_id[i].chain == ' ');
		ck_assert_int_eq(ag->residue_id[i].residue_seq, 1);
		ck_assert(ag->residue_id[i].insertion == ' ');

		struct mol_residue *res = mol_atom_residue(ag, i);
		ck_assert(res != NULL);
		ck_assert(i >= res->atom_start);
		ck_assert(i <= res->atom_end);
		ck_assert(ag->residue_id[i].chain == res->residue_id.chain);
		ck_assert(ag->residue_id[i].residue_seq == res->residue_id.residue_seq);
		ck_assert(ag->residue_id[i].insertion == res->residue_id.insertion);
	}

	ck_assert(ag->residue_list[0] != NULL);
	ck_assert_int_eq(ag->residue_list[0]->atom_start, 0);
	ck_assert_int_eq(ag->residue_list[0]->atom_end, 45);
	ck_assert_int_eq(ag->residue_list[0]->residue_id.residue_seq, 1);
	ck_assert_int_eq(mol_residue_id_cmp(&ag->residue_list[0]->residue_id, &ag->residue_id[0]), 0);

	free(mol_atom_group_fetch_metadata(ag, MOL_PDBQT_ATOM_TYPE_METADATA_KEY));
	mol_atom_group_free(ag);
}
END_TEST


START_TEST(test_guess_atom_types)
{
	// Please keep in mind: some donor/acceptor status in this file was changed manually for the sake of testing
	struct mol_atom_group *ag = mol_read_json("cats.json");
	ck_assert(ag != NULL);
	bool ret = mol_pdbqt_guess_atom_types(ag);
	ck_assert(ret == true);

	ck_assert(mol_atom_group_has_metadata(ag, MOL_PDBQT_ATOM_TYPE_METADATA_KEY));
	const enum mol_autodock_atom_type *autodock_type = \
		(const enum mol_autodock_atom_type *) \
		mol_atom_group_fetch_metadata(ag, MOL_PDBQT_ATOM_TYPE_METADATA_KEY);
	ck_assert_int_eq(autodock_type[0], AUTODOCK_ATOM_C);
	ck_assert_int_eq(autodock_type[1], AUTODOCK_ATOM_NA);
	ck_assert_int_eq(autodock_type[2], AUTODOCK_ATOM_N);
	ck_assert_int_eq(autodock_type[3], AUTODOCK_ATOM_C);
	ck_assert_int_eq(autodock_type[4], AUTODOCK_ATOM_C);
	ck_assert_int_eq(autodock_type[5], AUTODOCK_ATOM_C);
	ck_assert_int_eq(autodock_type[6], AUTODOCK_ATOM_C);
	ck_assert_int_eq(autodock_type[7], AUTODOCK_ATOM_NA);
	ck_assert_int_eq(autodock_type[8], AUTODOCK_ATOM_C);
	ck_assert_int_eq(autodock_type[9], AUTODOCK_ATOM_C);
	ck_assert_int_eq(autodock_type[10], AUTODOCK_ATOM_C);
	ck_assert_int_eq(autodock_type[11], AUTODOCK_ATOM_OA);
	ck_assert_int_eq(autodock_type[12], AUTODOCK_ATOM_C);
	ck_assert_int_eq(autodock_type[13], AUTODOCK_ATOM_N);
	ck_assert_int_eq(autodock_type[14], AUTODOCK_ATOM_C);
	ck_assert_int_eq(autodock_type[15], AUTODOCK_ATOM_C);
	ck_assert_int_eq(autodock_type[16], AUTODOCK_ATOM_C);
	ck_assert_int_eq(autodock_type[17], AUTODOCK_ATOM_C);
	ck_assert_int_eq(autodock_type[18], AUTODOCK_ATOM_C);
	ck_assert_int_eq(autodock_type[19], AUTODOCK_ATOM_C);
	ck_assert_int_eq(autodock_type[20], AUTODOCK_ATOM_C);
	ck_assert_int_eq(autodock_type[21], AUTODOCK_ATOM_C);
	ck_assert_int_eq(autodock_type[22], AUTODOCK_ATOM_C);
	ck_assert_int_eq(autodock_type[23], AUTODOCK_ATOM_N);
	ck_assert_int_eq(autodock_type[24], AUTODOCK_ATOM_OA);
	ck_assert_int_eq(autodock_type[25], AUTODOCK_ATOM_C);
	ck_assert_int_eq(autodock_type[26], AUTODOCK_ATOM_C);
	ck_assert_int_eq(autodock_type[27], AUTODOCK_ATOM_N);
	ck_assert_int_eq(autodock_type[28], AUTODOCK_ATOM_C);
	ck_assert_int_eq(autodock_type[29], AUTODOCK_ATOM_C);
	ck_assert_int_eq(autodock_type[30], AUTODOCK_ATOM_C);
	ck_assert_int_eq(autodock_type[31], AUTODOCK_ATOM_C);
	ck_assert_int_eq(autodock_type[32], AUTODOCK_ATOM_C);
	ck_assert_int_eq(autodock_type[33], AUTODOCK_ATOM_C);
	ck_assert_int_eq(autodock_type[34], AUTODOCK_ATOM_C);
	ck_assert_int_eq(autodock_type[35], AUTODOCK_ATOM_C);
	ck_assert_int_eq(autodock_type[36], AUTODOCK_ATOM_F);
	ck_assert_int_eq(autodock_type[37], AUTODOCK_ATOM_F);
	ck_assert_int_eq(autodock_type[38], AUTODOCK_ATOM_F);
	ck_assert_int_eq(autodock_type[39], AUTODOCK_ATOM_S);
	ck_assert_int_eq(autodock_type[40], AUTODOCK_ATOM_A);
	ck_assert_int_eq(autodock_type[41], AUTODOCK_ATOM_OA);
	ck_assert_int_eq(autodock_type[42], AUTODOCK_ATOM_O);
	ck_assert_int_eq(autodock_type[43], AUTODOCK_ATOM_HD);
	ck_assert_int_eq(autodock_type[44], AUTODOCK_ATOM_HD);
	ck_assert_int_eq(autodock_type[45], AUTODOCK_ATOM_HD);
	ck_assert_int_eq(autodock_type[46], AUTODOCK_ATOM_H);

	free(mol_atom_group_fetch_metadata(ag, MOL_PDBQT_ATOM_TYPE_METADATA_KEY));
	mol_atom_group_free(ag);
}
END_TEST


START_TEST(test_guess_no_hbond)
{
	struct mol_atom_group *ag = mol_read_pdb("1rei.pdb");
	ck_assert(ag != NULL);
	bool ret = mol_pdbqt_guess_atom_types(ag);

	ck_assert(ret == false);
	ck_assert(!mol_atom_group_has_metadata(ag, MOL_PDBQT_ATOM_TYPE_METADATA_KEY));
	mol_atom_group_free(ag);
}
END_TEST


START_TEST(test_guess_long_element_name)
{
	// Check that we behave sensibly on weird PDB names
	struct mol_atom_group *ag = mol_read_json("cats.json");
	ck_assert(ag != NULL);

	free(ag->element[0]);
	char *uuuhq = "Uuuuuuuhq"; // Element 111111164
	ag->element[0] = uuuhq;

	bool ret = mol_pdbqt_guess_atom_types(ag);
	ck_assert(ret == true);

	ck_assert(mol_atom_group_has_metadata(ag, MOL_PDBQT_ATOM_TYPE_METADATA_KEY));
	const enum mol_autodock_atom_type *autodock_type = \
		(const enum mol_autodock_atom_type *) \
		mol_atom_group_fetch_metadata(ag, MOL_PDBQT_ATOM_TYPE_METADATA_KEY);

	ck_assert_int_eq(autodock_type[0], AUTODOCK_ATOM_X);
	ck_assert_int_eq(autodock_type[1], AUTODOCK_ATOM_NA);

	ag->element[0] = NULL;
	free(mol_atom_group_fetch_metadata(ag, MOL_PDBQT_ATOM_TYPE_METADATA_KEY));
	mol_atom_group_free(ag);
}
END_TEST


START_TEST(test_read_protein)
{
	// File produced by Babel.
	// For the testing, edited alternate location, occupancy, and beta for atom 560
	struct mol_atom_group *ag = mol_pdbqt_read("1rei.pdbqt");
	ck_assert(ag != NULL);
	ck_assert_int_eq(ag->natoms, 1707);
	//ATOM    560  N  AILE A  2      -10.122  46.639   2.897  0.50  8.70    -0.197 NA
	ck_assert_str_eq(ag->atom_name[559], " N  ");
	ck_assert_str_eq(ag->element[559], "N");
	ck_assert(ag->alternate_location[559] == 'A');
	ck_assert_str_eq(ag->residue_name[559], "ILE");
	ck_assert(ag->residue_id[559].chain == 'A');
	ck_assert_int_eq(ag->residue_id[559].residue_seq, 2);
	ck_assert(ag->residue_id[559].insertion == ' ');
	ck_assert(ag->coords[559].X == -10.122);
	ck_assert(ag->coords[559].Y == 46.639);
	ck_assert(ag->coords[559].Z == 2.897);
	ck_assert(ag->occupancy[559] == 0.50);
	ck_assert(ag->B[559] == 8.70);
	ck_assert(ag->charge[559] == -0.197);

	ck_assert(mol_atom_group_has_metadata(ag, MOL_PDBQT_ATOM_TYPE_METADATA_KEY));
	const enum mol_autodock_atom_type *autodock_type = \
		(const enum mol_autodock_atom_type *) \
			mol_atom_group_fetch_metadata(ag, MOL_PDBQT_ATOM_TYPE_METADATA_KEY);
	ck_assert_int_eq(autodock_type[559], AUTODOCK_ATOM_NA);

	for (size_t i = 0; i < ag->natoms; i++) {
		struct mol_residue *res = mol_atom_residue(ag, i);
		ck_assert(res != NULL);
		ck_assert(i >= res->atom_start);
		ck_assert(i <= res->atom_end);
		ck_assert(ag->residue_id[i].chain == res->residue_id.chain);
		ck_assert(ag->residue_id[i].residue_seq == res->residue_id.residue_seq);
		ck_assert(ag->residue_id[i].insertion == res->residue_id.insertion);
	}

	for (size_t i = 0; i < ag->nresidues - 1; i++) {
		struct mol_residue *a = ag->residue_list[i];
		struct mol_residue *b = ag->residue_list[i+1];

		ck_assert(a != NULL);
		ck_assert(b != NULL);

		ck_assert(mol_residue_id_cmp(&a->residue_id,
		                             &b->residue_id) == -1);
	}

	free(mol_atom_group_fetch_metadata(ag, MOL_PDBQT_ATOM_TYPE_METADATA_KEY));
	mol_atom_group_free(ag);
}
END_TEST


START_TEST(test_read_nonexistent)
{
	struct mol_atom_group *ag = mol_pdbqt_read("nonexistent");
	ck_assert(ag == NULL);
}
END_TEST


START_TEST(test_read_no_atoms)
{
	// It's pdb, not pdbqt. But since there are no atoms, what's the difference?
	struct mol_atom_group *ag = mol_pdbqt_read("no_atoms.pdb");
	ck_assert(ag == NULL);
}
END_TEST


Suite *atom_group_suite(void)
{
	Suite *suite = suite_create("pdbqt_reading");

	TCase *tcase_read = tcase_create("test_read");
	tcase_add_test(tcase_read, test_read_small_molecule);
	tcase_add_test(tcase_read, test_read_protein);

	TCase *tcase_guess = tcase_create("test_guess");
	tcase_add_test(tcase_guess, test_guess_atom_types);
	tcase_add_test(tcase_guess, test_guess_no_hbond);
	tcase_add_test(tcase_guess, test_guess_long_element_name);

	TCase *tcase_errors = tcase_create("test_read_errors");
	tcase_add_test(tcase_errors, test_read_nonexistent);
	tcase_add_test(tcase_errors, test_read_no_atoms);

	suite_add_tcase(suite, tcase_read);
	suite_add_tcase(suite, tcase_guess);
	suite_add_tcase(suite, tcase_errors);

	return suite;
}


int main(void)
{
	Suite *suite = atom_group_suite();
	SRunner *runner = srunner_create(suite);
	srunner_run_all(runner, CK_ENV);

	int number_failed = srunner_ntests_failed(runner);
	srunner_free(runner);
	return number_failed;
}
