#include <stdio.h>
#include <check.h>
#include <errno.h>
#include <math.h>

#include "benergy.h"
#include "pdb.h"
#include "icharmm.h"

struct mol_atom_group *test_ag;
const double delta = 0.000001;
const double tolerance = 1e-3;
const double tolerance_strict = 1e-9;

void check_b_grads(struct mol_atom_group *ag, double d,
		   void (*efun) (struct mol_atom_group *, double *))
{
	double en, en1, t;
	struct mol_vector3 *fs = calloc(ag->natoms, sizeof(struct mol_vector3));
//en0
	en = 0;
	(*efun) (ag, &en);

	for (size_t i = 0; i < ag->natoms; i++) {
//x
		en1 = 0;
		t = ag->coords[i].X;
		ag->coords[i].X = d + t;
		(*efun) (ag, &en1);
		ag->coords[i].X = t;
		fs[i].X = (en - en1) / d;
//y
		en1 = 0;
		t = ag->coords[i].Y;
		ag->coords[i].Y = d + t;
		(*efun) (ag, &en1);
		ag->coords[i].Y = t;
		fs[i].Y = (en - en1) / d;
//z
		en1 = 0;
		t = ag->coords[i].Z;
		ag->coords[i].Z = d + t;
		(*efun) (ag, &en1);
		ag->coords[i].Z = t;
		fs[i].Z = (en - en1) / d;
	}
	en = 0;
	mol_zero_gradients(ag);
	(*efun) (ag, &en);
	char msg[256];

	for (size_t i = 0; i < ag->natoms; i++) {
		sprintf(msg,
			"\n(atom: %ld) calc: (%lf, %lf, %lf): numerical: (%lf, %lf, %lf)\n",
			i,
			ag->gradients[i].X, ag->gradients[i].Y, ag->gradients[i].Z,
			fs[i].X, fs[i].Y, fs[i].Z);
		ck_assert_msg(fabs(ag->gradients[i].X - fs[i].X) < tolerance, msg);
		ck_assert_msg(fabs(ag->gradients[i].Y - fs[i].Y) < tolerance, msg);
		ck_assert_msg(fabs(ag->gradients[i].Z - fs[i].Z) < tolerance, msg);
	}
	free(fs);
}

void setup(void)
{
	test_ag = mol_read_pdb("phenol.pdb");
	mol_atom_group_read_geometry(test_ag, "phenol.psf", "small.prm", "small.rtf");

	test_ag->gradients = calloc(test_ag->natoms, sizeof(struct mol_vector3));
	mol_fixed_init(test_ag);
	mol_fixed_update(test_ag, 0, NULL); // Make nothing fixed
}

void teardown(void)
{
	mol_atom_group_free(test_ag);
}

// Test cases
START_TEST(test_beng)
{
	mol_zero_gradients(test_ag);
	check_b_grads(test_ag, delta, beng);
}
END_TEST

START_TEST(test_aeng)
{
	mol_zero_gradients(test_ag);
	check_b_grads(test_ag, delta, aeng);
}
END_TEST

START_TEST(test_ieng)
{
	mol_zero_gradients(test_ag);
	check_b_grads(test_ag, delta, ieng);
}
END_TEST

START_TEST(test_teng)
{
	mol_zero_gradients(test_ag);
	check_b_grads(test_ag, delta, teng);
}
END_TEST

//c-c extended by 0.1A along x-axis
//c-o extended by 0.1A along y-axis (in negative direction)
//other c-o extended by 0.1A along z-axis
START_TEST(acetate_sprung)
{
	test_ag = mol_read_pdb("acetate_sprung.pdb");
	mol_atom_group_read_geometry(test_ag, "acetate.psf", "small.prm", "small.rtf");

	test_ag->gradients = calloc(test_ag->natoms, sizeof(struct mol_vector3));
	mol_fixed_init(test_ag);
	mol_fixed_update(test_ag, 0, NULL); // Make nothing fixed

	double energy = 0;
	mol_zero_gradients(test_ag);
	beng(test_ag, &energy);
	double predicted_energy = (328.30+648.00+648.00)*(0.1*0.1); //based on small.prm
	char msg[256];
	sprintf(msg, "acetate energy: calculated: %lf predicted: %lf\n", energy,
			predicted_energy);
	ck_assert_msg(fabs(predicted_energy - energy) < tolerance_strict, msg);
	double predicted_0_gX = -2.0*328.30*0.1; //based on small.prm
	double predicted_1_gX = 2.0*328.30*0.1; //based on small.prm
	double predicted_1_gY = -2.0*648.00*0.1; //based on small.prm
	double predicted_1_gZ = 2.0*648.00*0.1; //based on small.prm
	double predicted_2_gY = 2.0*648.00*0.1; //based on small.prm
	double predicted_3_gZ = -2.0*648.00*0.1; //based on small.prm

	sprintf(msg, "acetate gradient: (atom %d direction: %c)  calculated: %lf predicted: %lf\n",
			0, 'X', test_ag->gradients[0].X, predicted_0_gX);
	ck_assert_msg(fabs(predicted_0_gX - test_ag->gradients[0].X) < tolerance_strict, msg);

	sprintf(msg, "acetate gradient: (atom %d direction: %c)  calculated: %lf predicted: %lf\n",
			1, 'X', test_ag->gradients[1].X, predicted_1_gX);
	ck_assert_msg(fabs(predicted_1_gX - test_ag->gradients[1].X) < tolerance_strict, msg);

	sprintf(msg, "acetate gradient: (atom %d direction: %c)  calculated: %lf predicted: %lf\n",
			1, 'Y', test_ag->gradients[1].Y, predicted_1_gY);
	ck_assert_msg(fabs(predicted_1_gY - test_ag->gradients[1].Y) < tolerance_strict, msg);

	sprintf(msg, "acetate gradient: (atom %d direction: %c)  calculated: %lf predicted: %lf\n",
			1, 'Z', test_ag->gradients[1].Z, predicted_1_gZ);
	ck_assert_msg(fabs(predicted_1_gZ - test_ag->gradients[1].Z) < tolerance_strict, msg);

	sprintf(msg, "acetate gradient: (atom %d direction: %c)  calculated: %lf predicted: %lf\n",
			2, 'Y', test_ag->gradients[2].Y, predicted_2_gY);
	ck_assert_msg(fabs(predicted_2_gY - test_ag->gradients[2].Y) < tolerance_strict, msg);

	sprintf(msg, "acetate gradient: (atom %d direction: %c)  calculated: %lf predicted: %lf\n",
			3, 'Z', test_ag->gradients[3].Z, predicted_3_gZ);
	ck_assert_msg(fabs(predicted_3_gZ - test_ag->gradients[3].Z) < tolerance_strict, msg);

	mol_atom_group_free(test_ag);
}
END_TEST

//c-c shortened by 0.1A along x-axis
//c-o shortened by 0.1A along y-axis (in negative direction)
//other c-o shortened by 0.1A along z-axis
START_TEST(acetate_sprang)
{
	test_ag = mol_read_pdb("acetate_sprang.pdb");
	mol_atom_group_read_geometry(test_ag, "acetate.psf", "small.prm", "small.rtf");

	test_ag->gradients = calloc(test_ag->natoms, sizeof(struct mol_vector3));
	mol_fixed_init(test_ag);
	mol_fixed_update(test_ag, 0, NULL); // Make nothing fixed

	double energy = 0;
	mol_zero_gradients(test_ag);
	beng(test_ag, &energy);
	double predicted_energy = (328.30+648.00+648.00)*(0.1*0.1); //based on small.prm
	char msg[256];
	sprintf(msg, "acetate energy: calculated: %lf predicted: %lf\n", energy,
			predicted_energy);
	ck_assert_msg(fabs(predicted_energy - energy) < tolerance_strict, msg);
	double predicted_0_gX = 2.0*328.30*0.1; //based on small.prm
	double predicted_1_gX = -2.0*328.30*0.1; //based on small.prm
	double predicted_1_gY = 2.0*648.00*0.1; //based on small.prm
	double predicted_1_gZ = -2.0*648.00*0.1; //based on small.prm
	double predicted_2_gY = -2.0*648.00*0.1; //based on small.prm
	double predicted_3_gZ = 2.0*648.00*0.1; //based on small.prm

	sprintf(msg, "acetate gradient: (atom %d direction: %c)  calculated: %lf predicted: %lf\n",
			0, 'X', test_ag->gradients[0].X, predicted_0_gX);
	ck_assert_msg(fabs(predicted_0_gX - test_ag->gradients[0].X) < tolerance_strict, msg);

	sprintf(msg, "acetate gradient: (atom %d direction: %c)  calculated: %lf predicted: %lf\n",
			1, 'X', test_ag->gradients[1].X, predicted_1_gX);
	ck_assert_msg(fabs(predicted_1_gX - test_ag->gradients[1].X) < tolerance_strict, msg);

	sprintf(msg, "acetate gradient: (atom %d direction: %c)  calculated: %lf predicted: %lf\n",
			1, 'Y', test_ag->gradients[1].Y, predicted_1_gY);
	ck_assert_msg(fabs(predicted_1_gY - test_ag->gradients[1].Y) < tolerance_strict, msg);

	sprintf(msg, "acetate gradient: (atom %d direction: %c)  calculated: %lf predicted: %lf\n",
			1, 'Z', test_ag->gradients[1].Z, predicted_1_gZ);
	ck_assert_msg(fabs(predicted_1_gZ - test_ag->gradients[1].Z) < tolerance_strict, msg);

	sprintf(msg, "acetate gradient: (atom %d direction: %c)  calculated: %lf predicted: %lf\n",
			2, 'Y', test_ag->gradients[2].Y, predicted_2_gY);
	ck_assert_msg(fabs(predicted_2_gY - test_ag->gradients[2].Y) < tolerance_strict, msg);

	sprintf(msg, "acetate gradient: (atom %d direction: %c)  calculated: %lf predicted: %lf\n",
			3, 'Z', test_ag->gradients[3].Z, predicted_3_gZ);
	ck_assert_msg(fabs(predicted_3_gZ - test_ag->gradients[3].Z) < tolerance_strict, msg);

	mol_atom_group_free(test_ag);
}
END_TEST

Suite *benergy_suite(void)
{
	Suite *suite = suite_create("benergy");

	// Add test cases here
	// Each test case can call multiple test functions
	// Too add a test case, call tcase_add_test
	// The first argument is the TCase struct, the second is the
	//  test function name.
	TCase *tcase = tcase_create("test");
	tcase_set_timeout(tcase, 20);
        tcase_add_checked_fixture(tcase, setup, teardown);
	tcase_add_test(tcase, test_beng);
	tcase_add_test(tcase, test_aeng);
	tcase_add_test(tcase, test_ieng);
	tcase_add_test(tcase, test_teng);

	suite_add_tcase(suite, tcase);

	TCase *bonds = tcase_create("bond_acetate");
	tcase_add_test(bonds, acetate_sprung);
	tcase_add_test(bonds, acetate_sprang);

	suite_add_tcase(suite, bonds);

	return suite;
}

int main(void)
{
	Suite *suite = benergy_suite();
	SRunner *runner = srunner_create(suite);
	srunner_run_all(runner, CK_ENV);

	int number_failed = srunner_ntests_failed(runner);
	srunner_free(runner);
	return number_failed;
}
