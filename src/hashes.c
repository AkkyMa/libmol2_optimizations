#define _POSIX_C_SOURCE 200809L
#include "hashes.h"

__KHASH_IMPL(STR, , const char *, void *, 1,
            kh_str_hash_func, kh_str_hash_equal)
