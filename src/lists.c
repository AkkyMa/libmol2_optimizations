#define _POSIX_C_SOURCE 200809L
#include "lists.h"
#include "utils.h"

void mol_index_list_union(struct mol_index_list *dst,
			const struct mol_index_list *a, const struct mol_index_list *b,
			bool _realloc)
{
	dst->size = 0;

	size_t i = 0, j = 0;
	while (i < a->size && j < b->size) {
		if (a->members[i] < b->members[j]) {
			dst->members[dst->size] = a->members[i];
			++i;
		} else if (a->members[i] > b->members[j]) {
			dst->members[dst->size] = b->members[j];
			++j;
		} else {
			dst->members[dst->size] = a->members[i];
			++i;
			++j;
		}

		++dst->size;
	}

	if (i < a->size) {
		while(i < a->size) {
			dst->members[dst->size] = a->members[i];
			++dst->size;
			++i;
		}
	} else if (j < b->size) {
		while(j < b->size) {
			dst->members[dst->size] = b->members[j];
			++dst->size;
			++j;
		}
	}

	if (_realloc) {
		dst->members = realloc(dst->members, (dst->size)*sizeof(size_t));
	}
}
