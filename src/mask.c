#include <errno.h>
#include <stdbool.h>

#include "mask.h"

void mol_atom_group_mask(struct mol_atom_group *target,
			 const struct mol_atom_group *mask,
			 double maskr)
{
	if (target == NULL || mask == NULL) {
		errno = EFAULT;
		return;
	}

	if (target->mask == NULL) {
		target->mask = calloc(target->natoms, sizeof(bool));
	}

	double maskr_sq = maskr * maskr;

	for (size_t i = 0; i < target->natoms; i++) {
		for (size_t j = 0; j < mask->natoms; j++) {
			bool masked = MOL_VEC_EUCLIDEAN_DIST_SQ(target->coords[i], mask->coords[j]) < maskr_sq;
			if (masked) {
				target->mask[i] = true;
				break;
			}
		}
	}
}
