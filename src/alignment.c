#define _POSIX_C_SOURCE 200809L
#define _GNU_SOURCE
#include "alignment.h"
#include "sequence.h"
#include "utils.h"
#ifdef _WIN32
#define strdup _strdup
#endif

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

struct mol_traceback_matrix *new_mol_traceback_matrix(char *seq_columns,
		char *seq_rows)
{
	if (seq_columns == NULL || seq_rows == NULL) {
		return NULL;
	}

	size_t columns = strlen(seq_columns) + 1;
	size_t rows = strlen(seq_rows) + 1;

	struct mol_traceback_matrix *matrix = malloc(sizeof(struct mol_traceback_matrix));
	matrix->entries = calloc(rows * columns, sizeof(struct mol_traceback_entry));

	matrix->ncolumns = columns;
	matrix->nrows = rows;

	matrix->seq_columns = strdup(seq_columns);
	matrix->seq_rows = strdup(seq_rows);

	return matrix;
}

void destroy_mol_traceback_matrix(struct mol_traceback_matrix *matrix)
{
	if (matrix != NULL) {
		free_if_not_null(matrix->entries);
		free_if_not_null(matrix->seq_columns);
		free_if_not_null(matrix->seq_rows);
	}
}

void free_mol_traceback_matrix(struct mol_traceback_matrix *matrix)
{
	if (matrix != NULL) {
		destroy_mol_traceback_matrix(matrix);
		free(matrix);
	}
}

struct mol_alignment *global_alignment(struct mol_traceback_matrix *matrix)
{
	if (matrix == NULL || matrix->entries == NULL || matrix->seq_columns == NULL
			|| matrix->seq_rows == NULL) {
		return NULL;
	}

	const size_t lower_right = matrix->nrows * matrix->ncolumns - 1;

	size_t cur_i = matrix->nrows - 1;
	size_t cur_j = matrix->ncolumns - 1;

	//find length of alignment
	size_t len = 0;
	size_t cur_index = lower_right;
	while (cur_index != 0) {
		struct mol_traceback_entry *entry = &(matrix->entries[cur_index]);
		if (entry->dir == MOL_T_LEFT) {
			cur_j--;
		} else if (entry->dir == MOL_T_UP) {
			cur_i--;
		} else if (entry->dir == MOL_T_DIAGONAL) {
			cur_i--;
			cur_j--;
		}

		len++;
		cur_index = cur_i * matrix->ncolumns + cur_j;
	}

	char *seq1 = calloc(len+1, sizeof(char));
	char *seq2 = calloc(len+1, sizeof(char));

	cur_i = matrix->nrows - 1;
	cur_j = matrix->ncolumns - 1;
	cur_index = lower_right;
	while (cur_index != 0) {
		len--; //decrement length to write alignment in reverse order

		struct mol_traceback_entry *entry = &(matrix->entries[cur_index]);
		if (entry->dir == MOL_T_LEFT) {
			cur_j--;
			seq1[len] = matrix->seq_columns[cur_j];
			seq2[len] = '-';
		} else if (entry->dir == MOL_T_UP) {
			cur_i--;
			seq1[len] = '-';
			seq2[len] = matrix->seq_rows[cur_i];
		} else if (entry->dir == MOL_T_DIAGONAL) {
			cur_i--;
			cur_j--;
			seq1[len] = matrix->seq_columns[cur_j];
			seq2[len] = matrix->seq_rows[cur_i];
		}

		cur_index = cur_i * matrix->ncolumns + cur_j;
	}

	struct mol_alignment *aln = malloc(sizeof(struct mol_alignment));
	aln->seq1 = seq1;
	aln->seq2 = seq2;

	return aln;
}

struct mol_alignment *mol_align_to_seqres(struct mol_atom_group *ag)
{
	if (ag == NULL || ag->seqres == NULL) {
		return NULL;
	}

	const int gap_penalty = -8;

	char *atom_seq = mol_atom_group_sequence(ag);

	struct mol_traceback_matrix *matrix = new_mol_traceback_matrix(ag->seqres, atom_seq);

	bool *gaps = mol_gaps(ag);

	/* construct traceback matrix
	 *
	 * Matrix rules:
	 * (1) No gaps in seqres -> no moving up in the traceback
	 * (2) No gap pentalty where gaps have been found by mol_gaps
	 * (3) No gap penalty at beginning of sequence
	 * (4) Identity only -> no mutations allowed in SEQRES compared
	 * to atoms.
	 */

	struct mol_traceback_entry *root = &(matrix->entries[0]);
	root->score = 0;
	root->dir = MOL_T_ROOT;

	for (size_t i = 1; i < matrix->ncolumns; i++) {
		struct mol_traceback_entry *entry = &(matrix->entries[i]);
		entry->score = 0;
		entry->dir = MOL_T_LEFT;
	}

	//construct first column. Although the rules disallow a traceback with gaps
	//here, we construct it anyway.
	for (size_t i = 1; i < matrix->nrows; i++) {
		size_t index = i *  matrix->ncolumns;
		struct mol_traceback_entry *entry = &(matrix->entries[index]);
		entry->score = -20 * i; //gaps before seqres have massive negative
		entry->dir = MOL_T_UP;
	}

	size_t gap_position = 0;
	for (size_t i = 1; i < matrix->nrows; i++) {
		size_t atom_seq_position = i-1;
		for (size_t j = 1; j < matrix->ncolumns; j++) {
			size_t seqres_position = j-1;
			size_t index = i *  matrix->ncolumns + j;

			int score_left = matrix->entries[index-1].score;
			if (gaps[gap_position]) {
				//check if we are opening the gap
				if (matrix->entries[index-1].dir != MOL_T_LEFT) {
					score_left += 3;
				}
			} else {
				score_left += gap_penalty;
			}

			size_t diagonal_index = (i-1) * matrix->ncolumns + (j-1);
			int score_diagonal =  matrix->entries[diagonal_index].score;
			if (ag->seqres[seqres_position] == atom_seq[atom_seq_position]) {
				int match_score = 2;
				if (gap_position > 0 && gaps[gap_position-1] &&
						atom_seq[atom_seq_position-1] != '/') {
					//check if gap did not occur
					if (matrix->entries[diagonal_index].dir != MOL_T_LEFT) {
						match_score = -1;
					}
				}
				score_diagonal += match_score;
			} else {
				score_diagonal -= 1;
			}

			struct mol_traceback_entry *entry = &(matrix->entries[index]);
			if (score_left >= score_diagonal) {
				entry->score = score_left;
				entry->dir = MOL_T_LEFT;
			} else {
				entry->score = score_diagonal;
				entry->dir = MOL_T_DIAGONAL;
			}
		}

		//increment gap position unless we are at a chain break
		if (atom_seq[i] != '/') {
			gap_position++;
		}
	}

	struct mol_alignment *aln = global_alignment(matrix);
	free(atom_seq);
	free(gaps);
	free_mol_traceback_matrix(matrix);

	return aln;
}

void destroy_mol_alignment(struct mol_alignment *alignment)
{
	if (alignment != NULL) {
		free_if_not_null(alignment->seq1);
		free_if_not_null(alignment->seq2);
	}
}
void free_mol_alignment(struct mol_alignment *alignment)
{
	if (alignment != NULL) {
		destroy_mol_alignment(alignment);
		free(alignment);
	}
}
