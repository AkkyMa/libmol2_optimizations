#define _POSIX_C_SOURCE 200809L
#define _GNU_SOURCE
#define _USE_MATH_DEFINES
#include "quaternion.h"
#include "private_utils.h"

#include <math.h>
#include <stdio.h>
#include <stdbool.h>


struct mol_quaternion *mol_quaternion_create(void)
{
	return calloc(1, sizeof(struct mol_quaternion));
}



struct mol_quaternion_list *mol_quaternion_list_create(const size_t n)
{
	struct mol_quaternion_list *list =
	     malloc(sizeof(struct mol_quaternion_list));
	list->size = n;
	list->members = calloc(n, sizeof(struct mol_quaternion));
	return list;
}


static struct mol_quaternion_list *_list_from_file(const char *filename, const bool normalize)
{
	struct mol_quaternion_list *rotset = NULL;
	FILE *fp = fopen(filename, "r");

	if (fp == NULL) {
		perror(__func__);
		_PRINTF_ERROR("Unable to open: %s", filename);
		goto nofile_out;
	}

	char *line = NULL;
	size_t len = 0;

	size_t nrots = 0;

	while (getline(&line, &len, fp) != -1) {
		if (!is_whitespace_line(line))
			++nrots;
	}

	if (nrots == 0) {
		_PRINTF_ERROR("No rotations found in %s", filename);
		goto reading_out;
	}

	rewind(fp);

	rotset = mol_quaternion_list_create(nrots);

	size_t i = 0;
	while (getline(&line, &len, fp) != -1) {
		if (!is_whitespace_line(line)) {
			int tokens_read = sscanf(line, "%lf %lf %lf %lf",
			                         &rotset->members[i].W,
			                         &rotset->members[i].X,
			                         &rotset->members[i].Y,
			                         &rotset->members[i].Z);
			if (tokens_read != 4) {
				_PRINTF_ERROR("Malformed line in %s", filename);
				mol_list_free(rotset);
				rotset = NULL;
				goto reading_out;
			}
			if (normalize) {
				rotset->members[i] = mol_quaternion_normalize(rotset->members[i]);
			}
			i++;
		}
	}

reading_out:
	free(line);
	fclose(fp);
nofile_out:
	return rotset;
}


struct mol_quaternion_list *mol_quaternion_list_from_file(const char *filename)
{
	return _list_from_file(filename, true);
}


struct mol_quaternion_list *mol_quaternion_list_from_file_unnormalized(const char *filename)
{
	return _list_from_file(filename, false);
}


void mol_quaternion_free(struct mol_quaternion *q)
{
	free(q);
}


double mol_quaternion_length(const struct mol_quaternion q)
{
	return sqrt(mol_quaternion_length_sq(q));
}


double mol_quaternion_length_sq(const struct mol_quaternion q)
{
	// Unlike mol_quaternion_length, there's no way to avoid overflow here if we have too large values.
	// So, this function is pretty straightforward.
	return q.W * q.W + q.X * q.X + q.Y * q.Y + q.Z * q.Z;
}


struct mol_quaternion mol_quaternion_normalize(const struct mol_quaternion q)
{
	double l = mol_quaternion_length(q);
	return mol_quaternion_scale(q, 1.0/l);
}

/*
 * mol_matrix3_from_quaternion and  mol_quaternion_from_matrix3
 * are derived from this identity based on a normalized quaternion:
 *
 *     | 1-2y^2-2z^2    2xy-2zw      2xz+2yw   |
 * R = |   2xy+2zw    1-2x^2-2z^2    2yz-2xw   |
 *     |   2xz-2yw      2yz+2xw    1-2x^2-2y^2 |
 */
void mol_matrix3_from_quaternion(struct mol_matrix3 *m, const struct mol_quaternion q)
{
	//here I intentionally use the unsafe method of squares rather than
	//hypot. If you wish to not risk overflow, you should normalize first
	double l2 = q.W * q.W + q.X * q.X + q.Y * q.Y + q.Z * q.Z;
	double s = (l2 == 0.0) ? 0.0 : 2.0 / l2;
	double wx = s * q.W * q.X;
	double wy = s * q.W * q.Y;
	double wz = s * q.W * q.Z;
	double xx = s * q.X * q.X;
	double xy = s * q.X * q.Y;
	double xz = s * q.X * q.Z;
	double yy = s * q.Y * q.Y;
	double yz = s * q.Y * q.Z;
	double zz = s * q.Z * q.Z;

	m->m11 = 1.0 - (yy + zz);
	m->m12 = xy - wz;
	m->m13 = xz + wy;
	m->m21 = xy + wz;
	m->m22 = 1.0 - (xx + zz);
	m->m23 = yz - wx;
	m->m31 = xz - wy;
	m->m32 = yz + wx;
	m->m33 = 1.0 - (xx + yy);
}


/*
 * mol_quaternion_from_matrix3 assumes the matrix
 * has the properties of a rotation matrix
 *
 * If there is a reason to believe the matrix is not
 * orthogonal, we use Version 3 of
 * https://doi.org/10.2514/2.4654
 */
void mol_quaternion_from_matrix3(struct mol_quaternion *q, const struct mol_matrix3 m)
{
	double trace = m.m11 + m.m22 + m.m33;

	if (trace >= 0.0) {
		double w2 = sqrt(1.0 + trace);
		double s = 0.5 / w2;

		q->W = 0.5 * w2;
		q->X = (m.m32 - m.m23) * s;
		q->Y = (m.m13 - m.m31) * s;
		q->Z = (m.m21 - m.m12) * s;
		goto norm;
	}
	// trace is negative, use techniques to reduce chance of sqrt(small number)
	// if calculating sqrt(small number, s will approach 0.5/0.0 with issues
	if ((m.m11 > m.m22) && (m.m11 > m.m33)) { // m11 is the biggest diagonal element
		double x2 = sqrt(1.0 + m.m11 - m.m22 - m.m33);
		double s = 0.5 / x2;

		q->W = (m.m32 - m.m23) * s;
		q->X = 0.5 * x2;
		q->Y = (m.m12 + m.m21) * s;
		q->Z = (m.m13 + m.m31) * s;
		goto norm;
	}

	if (m.m22 > m.m33) { // m22 is the biggest diagonal element
		double y2 = sqrt(1.0 + m.m22 - m.m11 - m.m33);
		double s = 0.5 / y2;

		q->W = (m.m13 - m.m31) * s;
		q->X = (m.m12 + m.m21) * s;
		q->Y = 0.5 * y2;
		q->Z = (m.m23 + m.m32) * s;
		goto norm;
	}
	// m33 is the biggest diagonal element
	double z2 = sqrt(1.0 + m.m33 - m.m11 - m.m22);
	double s = 0.5 / z2;

	q->W = (m.m21 - m.m12) * s;
	q->X = (m.m13 + m.m31) * s;
	q->Y = (m.m23 + m.m32) * s;
	q->Z = 0.5 * z2;

norm:
	*q = mol_quaternion_normalize(*q);
}


void mol_quaternion_from_axis_angle(struct mol_quaternion *q, const struct mol_vector3 *axis, const double angle)
{
	const double s = sin(angle/2);
	q->W = cos(angle/2);
	q->X = s * axis->X;
	q->Y = s * axis->Y;
	q->Z = s * axis->Z;
}

void mol_quaternion_to_axis_angle(struct mol_vector3 *axis, double *angle, const struct mol_quaternion *q)
{
	const double w = sqrt(MOL_VEC_SQ_NORM(*q));
	*angle = 2*atan2(w, q->W);
	if (*angle > M_PI) {
		*angle -= 2*M_PI;
	}
	MOL_VEC_COPY(*axis, *q); // Copy X, Y, Z
	if (w != 0) {
		MOL_VEC_DIV_SCALAR(*axis, *axis, w);
	} else { // If the rotation axis is undefined, set it to some arbitrary value.
		axis->X = 1;
		axis->Y = 0;
		axis->Z = 0;
	}
}


struct mol_quaternion mol_quaternion_scale(const struct mol_quaternion q, const double a)
{
	struct mol_quaternion qs;
	MOL_QUATERNION_MULT_SCALAR(qs, q, a);
	return qs;
}


struct mol_quaternion mol_quaternion_prod(
		const struct mol_quaternion qa,
		const struct mol_quaternion qb)
{
	struct mol_quaternion product;
	product.W = qa.W * qb.W - qa.X * qb.X - qa.Y * qb.Y - qa.Z * qb.Z;
	product.X = qa.W * qb.X + qa.X * qb.W + qa.Y * qb.Z - qa.Z * qb.Y;
	product.Y = qa.W * qb.Y + qa.Y * qb.W + qa.Z * qb.X - qa.X * qb.Z;
	product.Z = qa.W * qb.Z + qa.Z * qb.W + qa.X * qb.Y - qa.Y * qb.X;
	return product;
}


struct mol_quaternion mol_quaternion_conj(const struct mol_quaternion q)
{
	struct mol_quaternion qc = {.W = q.W, .X = -q.X, .Y = -q.Y, .Z = -q.Z};
	return qc;
}



struct mol_quaternion mol_quaternion_inv(const struct mol_quaternion q)
{
	const struct mol_quaternion qc = mol_quaternion_conj(q);
	const double length_sq = mol_quaternion_length_sq(q);
	return mol_quaternion_scale(qc, 1.0/length_sq);
}


// Deprecated functions

void mol_quaternion_norm(struct mol_quaternion *p, const struct mol_quaternion q)
{
	_PRINTF_DEPRECATION("mol_quaternion_normalize");
	*p = mol_quaternion_normalize(q);
}


void mol_quaternion_product(
	const struct mol_quaternion *qa,
	const struct mol_quaternion *qb,
	struct mol_quaternion *product)
{
	_PRINTF_DEPRECATION("mol_quaternion_prod");
	*product = mol_quaternion_prod(*qa, *qb);
}


void mol_quaternion_inverse(const struct mol_quaternion *q, struct mol_quaternion *q_inv)
{
	_PRINTF_DEPRECATION("mol_quaternion_invert");
	*q_inv = mol_quaternion_inv(*q);
}


void mol_quaternion_conjugate(const struct mol_quaternion *q, struct mol_quaternion *qc)
{
	_PRINTF_DEPRECATION("mol_quaternion_conj");
	*qc = mol_quaternion_conj(*q);
}


void mol_quaternion_scaled(const struct mol_quaternion *q, const double a, struct mol_quaternion *q_scaled)
{
	_PRINTF_DEPRECATION("mol_quaternion_scale");
	MOL_QUATERNION_MULT_SCALAR(*q_scaled, *q, a);
}
