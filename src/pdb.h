#ifndef _MOL_PDB_H_
#define _MOL_PDB_H_

#include <stdio.h>

#include "atom_group.h"

struct mol_atom_group *mol_read_pdb(const char *pdb_filename);
struct mol_atom_group *mol_fread_pdb(FILE *fp);
struct mol_atom_group_list *mol_read_pdb_models(const char *pdb_filename);

bool mol_write_pdb(const char *pdb_filename, const struct mol_atom_group *ag);
bool mol_fwrite_pdb(FILE *ofp, const struct mol_atom_group *ag);

/**
 * Parse a single line or PDB file
 * \param ag Atomgroup where the data will be stored.
 * \param atom_index Index of atom in the \p ag.
 * \param line Line to parse.
 * \param line_length Length of the line (not including NULL-terminator).
 */
void mol_pdb_parse_atom_line(
	struct mol_atom_group *ag,
	const size_t atom_index,
	const char *line,
	const ssize_t line_length);

/**
 * Check whether PDB line corresponds to an atom (starts with either ATOM or HETATM).
 * \param line Line to parse.
 * \return True if this is atom line (and should be parsable by \ref mol_pdb_parse_atom_line); false otherwise.
 */
bool mol_pdb_line_is_atom_or_hetatm(const char *line);

#endif /* _MOL_PDB_H_ */
