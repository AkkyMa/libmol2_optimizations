#ifndef _MOL_RIGID_H_
#define _MOL_RIGID_H_

#include "atom_group.h"
#include "vector.h"

struct mol_rigid_body {
	struct mol_vector3 center;
	double *origin;
};

void mol_atom_group_to_rigid_body( struct mol_rigid_body *rigidbody,
				struct mol_atom_group *ag);

void mol_rigid_body_to_atom_group(double *change, struct mol_atom_group *ag,
				struct mol_rigid_body *rigidbody);

void mol_rigidbody_grad(double *grad, struct mol_atom_group *ag, double *inp,
			double *origin);

#endif
