#include "fitting.h"

double mol_fitting_score(struct mol_atom_group *mob_ag,
                         const struct mol_atom_group *ref_ag,
                         const struct mol_fitting_params *prms,
                         const double scale)
{
	const double rad2 = prms->radius * prms->radius;
	double score = 0.;
	double d2, atom_score;

	for (size_t i = 0; i < mob_ag->natoms; i++) {
		for (size_t j = 0; j < ref_ag->natoms; j++) {
			struct mol_vector3 dist_vec;
			MOL_VEC_SUB(dist_vec, mob_ag->coords[i], ref_ag->coords[j]);

			d2 = MOL_VEC_SQ_NORM(dist_vec);
			atom_score = -1. * exp(-1. * d2 / rad2) * scale;
			score += atom_score;

			if (mob_ag->gradients != NULL) {
				struct mol_vector3 grad;
				double g_const = 2. * atom_score / rad2;
				MOL_VEC_MULT_SCALAR(grad, dist_vec, g_const);

				MOL_VEC_ADD(mob_ag->gradients[i], mob_ag->gradients[i], grad);
			}
		}
	}
	return score;
}


double mol_fitting_score_aglist(struct mol_atom_group *mob_ag,
                                struct mol_atom_group **ref_ag_list,
                                const size_t size,
                                const struct mol_fitting_params *prms,
                                const double scale)
{
	double score = 0.;

	for (size_t i = 0; i < size; i++) {
		score += mol_fitting_score(mob_ag, ref_ag_list[i], prms, scale);
	}

	return score;
}

