/*
  Copyright (c) 2013, Acpharis Inc
  All rights reserved.

  Redistribution and use in source and binary forms, with or without modification,
  are permitted provided that the following conditions are met:

  - Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.
  - Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.
  - Neither the name of the author nor the names of its contributors may be used
  to endorse or promote products derived from this software without specific
  prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
  IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
  OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef _MOL_VECTOR_H_
#define _MOL_VECTOR_H_

/**
 * \file vector.h
 */

struct mol_vector3f {
	float X;
	float Y;
	float Z;
};

struct mol_vector3 {
	double X;
	double Y;
	double Z;
};

struct mol_vector3l {
	long double X;
	long double Y;
	long double Z;
};

struct mol_vector3i {
	int X;
	int Y;
	int Z;
};

struct mol_vector3u {
	unsigned int X;
	unsigned int Y;
	unsigned int Z;
};


/// Add two vectors \p U and \p V element-wise, and put the result into \p DST.
#ifndef MOL_VEC_ADD
#define MOL_VEC_ADD(DST, U, V) do               \
	{					\
		(DST).X = (U).X + (V).X;	\
		(DST).Y = (U).Y + (V).Y;	\
		(DST).Z = (U).Z + (V).Z;	\
	} while(0)
#endif

/// Subtract two vectors \p U and \p V element-wise, and put the result into \p DST.
#ifndef MOL_VEC_SUB
#define MOL_VEC_SUB(DST, U, V) do               \
	{					\
		(DST).X = (U).X - (V).X;	\
		(DST).Y = (U).Y - (V).Y;	\
		(DST).Z = (U).Z - (V).Z;	\
	} while(0)
#endif

/// Multiply two vectors \p U and \p V element-wise, and put the result into \p DST.
#ifndef MOL_VEC_MULT
#define MOL_VEC_MULT(DST, U, V) do              \
	{					\
		(DST).X = (U).X * (V).X;	\
		(DST).Y = (U).Y * (V).Y;	\
		(DST).Z = (U).Z * (V).Z;	\
	} while(0)
#endif

/// Divide two vectors \p U and \p V element-wise, and pus the result into \p DST.
#ifndef MOL_VEC_DIV
#define MOL_VEC_DIV(DST, U, V) do               \
	{					\
		(DST).X = (U).X / (V).X;	\
		(DST).Y = (U).Y / (V).Y;	\
		(DST).Z = (U).Z / (V).Z;	\
	} while(0)
#endif


/// Add scalar constant \p C to all elements of \p U, and put the result into \p DST.
#ifndef MOL_VEC_ADD_SCALAR
#define MOL_VEC_ADD_SCALAR(DST, U, C) do        \
	{					\
		(DST).X = (U).X + (C);		\
		(DST).Y = (U).Y + (C);		\
		(DST).Z = (U).Z + (C);		\
	} while(0)
#endif

/// Subtract scalar constant \p C from all elements of \p U, and put the result into \p DST.
#ifndef MOL_VEC_SUB_SCALAR
#define MOL_VEC_SUB_SCALAR(DST, U, C) do        \
	{					\
		(DST).X = (U).X - (C);		\
		(DST).Y = (U).Y - (C);		\
		(DST).Z = (U).Z - (C);		\
	} while(0)
#endif

/// Multiply all elements of \p U by scalar constant \p C, and put the result into \p DST.
#ifndef MOL_VEC_MULT_SCALAR
#define MOL_VEC_MULT_SCALAR(DST, U, C) do       \
	{					\
		(DST).X = (U).X * (C);		\
		(DST).Y = (U).Y * (C);		\
		(DST).Z = (U).Z * (C);		\
	} while(0)
#endif

/// Divide all elements of \p U by scalar constant \p C, and put the result into \p DST.
#ifndef MOL_VEC_DIV_SCALAR
#define MOL_VEC_DIV_SCALAR(DST, U, C) do        \
	{					\
		(DST).X = (U).X / (C);		\
		(DST).Y = (U).Y / (C);		\
		(DST).Z = (U).Z / (C);		\
	} while(0)
#endif

/// Set all elements of \p DST to a scalar value \p C.
#ifndef MOL_VEC_SET_SCALAR
#define MOL_VEC_SET_SCALAR(DST, C) do           \
	{					\
		(DST).X = C;			\
		(DST).Y = C;			\
		(DST).Z = C;			\
	} while(0)
#endif

/**
 * Return the square of L2 norm. The return type will match the type of the vector given.
 */
#ifndef MOL_VEC_SQ_NORM
#define MOL_VEC_SQ_NORM(V)                      \
	((V).X*(V).X + (V).Y*(V).Y + (V).Z*(V).Z)
#endif

/**
 * Return the dot product of \p U and \p V. The return type will match the
 * type of the vectors given. If you pass in mixed vector types, will
 * return the "larger" type (int with float will return float, for example).
 */
#ifndef MOL_VEC_DOT_PROD
#define MOL_VEC_DOT_PROD(U, V)                  \
	((U).X*(V).X + (U).Y*(V).Y + (U).Z*(V).Z)
#endif

#ifndef MOL_VEC_CROSS_PROD
#define MOL_VEC_CROSS_PROD(DST, U, V) do		\
	{						\
		(DST).X = (U).Y*(V).Z - (V).Y*(U).Z;	\
		(DST).Y = (U).Z*(V).X - (V).Z*(U).X;	\
		(DST).Z = (U).X*(V).Y - (V).X*(U).Y;	\
	} while(0)
#endif

#ifndef MOL_VEC_EUCLIDEAN_DIST_SQ
#define MOL_VEC_EUCLIDEAN_DIST_SQ(U, V)					\
	(((U).X - (V).X)*((U).X - (V).X) +				\
	 ((U).Y - (V).Y)*((U).Y - (V).Y) +				\
	 ((U).Z - (V).Z)*((U).Z - (V).Z))
#endif

#ifndef MOL_VEC_EUCLIDEAN_DIST
#define MOL_VEC_EUCLIDEAN_DIST(U, V)            \
	sqrt(MOL_VEC_EUCLIDEAN_DIST_SQ(U, V))
#endif

/**
 * Check for equality of vectors.
 *
 * \warning Be careful when comparing floats.
 * \related MOL_VEC_NEQ
 */
#ifndef MOL_VEC_EQ
#define MOL_VEC_EQ(U, V)					\
	((U).X == (V).X && (U).Y == (V).Y && (U).Z == (V).Z)
#endif

/**
 * Check for inequality of vectors.
 *
  \warning Be careful when comparing floats.
 * \related MOL_VEC_EQ
 */
#ifndef MOL_VEC_NEQ
#define MOL_VEC_NEQ(U, V)					\
	((U).X != (V).X || (U).Y != (V).Y || (U).Z != (V).Z)
#endif

#ifndef MOL_VEC_LOWER_BOUND
#define MOL_VEC_LOWER_BOUND(V, BOUND)			\
	do {						\
		(V).X = (V).X < BOUND ? BOUND : (V).X;	\
		(V).Y = (V).Y < BOUND ? BOUND : (V).Y;	\
		(V).Z = (V).Z < BOUND ? BOUND : (V).Z;	\
	} while(0)
#endif

#ifndef MOL_VEC_MAX
#define MOL_VEC_MAX(DST, U, V)			                \
	do {					        	\
		(DST).X = (U).X < (V).X ? (V).X : (U).X;	\
		(DST).Y = (U).Y < (V).Y ? (V).Y : (U).Y;	\
		(DST).Z = (U).Z < (V).Z ? (V).Z : (U).Z;	\
	} while(0)
#endif

#ifndef MOL_VEC_MIN
#define MOL_VEC_MIN(DST, U, V)			                \
	do {					        	\
		(DST).X = (U).X < (V).X ? (U).X : (V).X;	\
		(DST).Y = (U).Y < (V).Y ? (U).Y : (V).Y;	\
		(DST).Z = (U).Z < (V).Z ? (U).Z : (V).Z;	\
	} while(0)
#endif

#ifndef MOL_VEC_APPLY
#define MOL_VEC_APPLY(U, FUNC) do		\
	{					\
		(U).X = FUNC((U).X);		\
		(U).Y = FUNC((U).Y);		\
		(U).Z = FUNC((U).Z);		\
	} while(0)
#endif

#ifndef MOL_VEC_COPY
#define MOL_VEC_COPY(DST, U) do               \
	{					\
		(DST).X = (U).X;	\
		(DST).Y = (U).Y;	\
		(DST).Z = (U).Z;	\
	} while(0)
#endif

#endif // _MOL_VECTOR_H_
