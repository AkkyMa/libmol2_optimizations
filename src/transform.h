#ifndef _MOL_TRANSFORM_H_
#define _MOL_TRANSFORM_H_

#include "atom_group.h"
#include "matrix.h"
#include "vector.h"
#include "quaternion.h"

void mol_vector3_translate(struct mol_vector3 *v, const struct mol_vector3 *tv);
void mol_vector3f_translate(struct mol_vector3f *v, const struct mol_vector3f *tv);
void mol_vector3l_translate(struct mol_vector3l *v, const struct mol_vector3l *tv);

void mol_vector3_rotate(struct mol_vector3 *v, const struct mol_matrix3 *r);
void mol_vector3f_rotate(struct mol_vector3f *v, const struct mol_matrix3f *r);
void mol_vector3l_rotate(struct mol_vector3l *v, const struct mol_matrix3l *r);

/**
 * This function rotates vector \p v according to quaternion \p q. All according to Wikipedia.
 * \param v[in,out] Vector to rotate.
 * \param q[in] Rotation quaternion.
 */
void mol_vector3_rotate_by_quaternion(struct mol_vector3 *v, const struct mol_quaternion *q);

void mol_vector3_translate_list(struct mol_vector3 *v_list, size_t len,
				const struct mol_vector3 *tv);
void mol_vector3f_translate_list(struct mol_vector3f *v_list, size_t len,
				const struct mol_vector3f *tv);
void mol_vector3l_translate_list(struct mol_vector3l *v_list, size_t len,
				const struct mol_vector3l *tv);

void mol_vector3_rotate_list(struct mol_vector3 *v_list, size_t len,
			const struct mol_matrix3 *r);
void mol_vector3f_rotate_list(struct mol_vector3f *v_list, size_t len,
			const struct mol_matrix3f *r);
void mol_vector3l_rotate_list(struct mol_vector3l *v_list, size_t len,
			const struct mol_matrix3l *r);

/**
 * These functions apply the rotation first, then the translation
 * */
void mol_vector3_move(struct mol_vector3 *v,
		      const struct mol_matrix3 *r,
		      const struct mol_vector3 *tv);
void mol_vector3f_move(struct mol_vector3f *v,
		       const struct mol_matrix3f *r,
		       const struct mol_vector3f *tv);
void mol_vector3l_move(struct mol_vector3l *v,
		       const struct mol_matrix3l *r,
		       const struct mol_vector3l *tv);

void mol_vector3_move_list(struct mol_vector3 *v_list, size_t len,
			   const struct mol_matrix3 *r,
			   const struct mol_vector3 *tv);
void mol_vector3f_move_list(struct mol_vector3f *v_list, size_t len,
			    const struct mol_matrix3f *r,
			    const struct mol_vector3f *tv);
void mol_vector3l_move_list(struct mol_vector3l *v_list, size_t len,
			    const struct mol_matrix3l *r,
			    const struct mol_vector3l *tv);

bool mol_get_transform_to_residue(
	struct mol_vector3 *translation, struct mol_matrix3 *rotation,
	const struct mol_atom_group *ag, const struct mol_residue *residue);

void mol_get_transform_to_atoms(
	struct mol_vector3 *translation, struct mol_matrix3 *rotation,
	const struct mol_atom_group *ag,
	size_t N_index, size_t CA_index, size_t C_index);

void mol_get_symmetry_transform(
	struct mol_vector3 *out_translation, struct mol_matrix3 *out_rotation,
	const struct mol_vector3 *in_translation,
	const struct mol_matrix3 *in_rotation,
	const unsigned int subunit);

#endif /* _MOL_TRANSFORM_H_ */
