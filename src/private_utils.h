#ifndef _MOL_PRIVATE_UTILS_H_
#define _MOL_PRIVATE_UTILS_H_

// This solution uses small hack: https://stackoverflow.com/q/5588855/929437, but it seems to be widely supported
// It also relies on __FILENAME__ macro defined in CMakeLists.txt
#define _FPRINTF_MSG(fd, type, fmt, ...) \
	fprintf(fd, "%s in libmol2 function %s (%s:%d): " fmt "\n", type, __func__, __FILENAME__, __LINE__, ##__VA_ARGS__)
#define _PRINTF_ERROR(fmt, ...) _FPRINTF_MSG(stderr, "ERROR", fmt, ##__VA_ARGS__)
#define _PRINTF_WARNING(fmt, ...) _FPRINTF_MSG(stderr, "WARNING", fmt, ##__VA_ARGS__)
#define _PRINTF_DEPRECATION(new) \
	fprintf(stderr, "WARNING libmol2 function '%s' has been deprecated! Use '%s' instead.\n", __func__, new)

#endif // _MOL_PRIVATE_UTILS_H_