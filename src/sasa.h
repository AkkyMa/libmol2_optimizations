#ifndef _MOL_SASA_H_
#define _MOL_SASA_H_

#include "atom_group.h"

void mol_mark_all_sa(struct mol_atom_group *ag);

void compute_surface(struct mol_atom_group *ag, double msur_k);

void baccs(struct mol_atom_group *ag, double r_solv, bool cont_acc,
	double sthresh);
void accs(double *as, const struct mol_atom_group *ag, float r_solv,
	bool cont_acc);

#endif /* _MOL_SASA_H_ */
