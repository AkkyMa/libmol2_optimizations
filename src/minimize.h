#ifndef _MOL_MINIMIZE_H_
#define _MOL_MINIMIZE_H_

/** \file minimize.h
        This file contains  functions
        for local structure minimization
*/

#include "atom_group.h"
#include "lbfgs.h"

enum mol_min_method
{
    MOL_LBFGS,
    MOL_CONJUGATE_GRADIENTS,
    MOL_POWELL,
    MOL_RIGID,
};


//Wrapper for atom group minimizers;
void mol_minimize_ag(enum mol_min_method min_type, unsigned int maxIt, double tol,
		struct mol_atom_group* ag, void* minprms, lbfgs_evaluate_t egfun);

#endif  // _MOL_MINIMIZE_H_
