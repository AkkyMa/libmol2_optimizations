#ifndef _MOL_ATOM_GROUP_MISSING_H_
#define _MOL_ATOM_GROUP_MISSING_H_

#include <float.h>
#include <math.h>
#include "vector.h"
#include "atom_group.h"

#define _vector_nan ((struct mol_vector3){.X = NAN, .Y = NAN, .Z = NAN})
#define _vector_zero ((struct mol_vector3){.X = 0, .Y = 0, .Z = 0})

// Below is some macro metaprogramming magic.

/*
 * Compile-time construct that returns true if `x` is of type `type`.
 * Using typeof is very problematic in ISO C11, so we resort to this thing.
 * For enums, it assumes that they are of type int.
 * I'm not sure this does not produce any false-positives, but at least if safe-guards from typos.
 */
#define _CMP_TYPE(x, type) _Generic((x), type: true, default: false)

/*
 * This macro checks that mol_atom_group field `name` is of type `type`.
 * Unlike C++, we can not write mol_atom_group::name here.
 * We need to create virtual instance of mol_atom_group, and reference its fields.
 * Since we don't actually access the fields, only reference them, this is NOT undefined behavior.
 */
#define _CMP_FIELD_TYPE(name, type) _CMP_TYPE((((struct mol_atom_group *)NULL)->name[0]), type)
/*
 * Do compile-time check that field `name` of mol_atom_group is of type `type`.
 * E.g., _ASSERT_TYPE(fixed, bool) will work, _ASSERT_TYPE(fixed, int) will cause compile error.
 */
#define _ASSERT_TYPE(name, type) _Static_assert(_CMP_FIELD_TYPE(name, type), \
	"Error when defining default values for "#name" field of mol_atom_group")

// Internal helper
#define _AG_FIELD(name, type, missing_internal, missing_output) \
	__attribute__((unused)) static const type mol_atom_group_## name ##_none = missing_internal; \
	__attribute__((unused)) static const type mol_atom_group_## name ## _default = missing_output

/*
 * Create definitions for missing values constants for atom_group field `name`.
 * Also, do compile-time type checking.
 */
#define AG_FIELD(name, type, missing_internal, missing_output) \
	_AG_FIELD(name, type, missing_internal, missing_output); \
	_ASSERT_TYPE(name, type)

/*
 * Same as AG_FIELD, but no type checking, since it does not work for enums.
 */
#define AG_FIELD_ENUM(name, type, missing_internal, missing_output) \
	_AG_FIELD(name, type, missing_internal, missing_output)


AG_FIELD(coords, struct mol_vector3, _vector_nan, _vector_zero);
AG_FIELD(gradients, struct mol_vector3, _vector_zero, _vector_zero);
AG_FIELD(element, char*, NULL, "X");
AG_FIELD(atom_name, char*, NULL, "X");
AG_FIELD(alternate_location, char, ' ', ' ');
AG_FIELD(residue_id, struct mol_residue_id,
         ((struct mol_residue_id){.residue_seq = 0, .chain = '\0', .insertion = ' '}),
         ((struct mol_residue_id){.residue_seq = 0, .chain = 'X', .insertion = ' '}));
AG_FIELD(residue_name, char*, NULL, "UNK");
AG_FIELD(occupancy, double, NAN, 0);
AG_FIELD(B, double, NAN, 0);
AG_FIELD(segment_id, char*, NULL, "X");
AG_FIELD(formal_charge, char*, NULL, "  ");
AG_FIELD_ENUM(record, enum mol_atom_record, MOL_RUNKNOWN, MOL_ATOM);
AG_FIELD(vdw_radius, double, NAN, 0);
AG_FIELD(vdw_radius03, double, NAN, 0);
AG_FIELD(eps, double, NAN, 0);
AG_FIELD(eps03, double, NAN, 0);
AG_FIELD(ace_volume, double, NAN, 0);
AG_FIELD(charge, double, NAN, 0);
AG_FIELD_ENUM(hbond_prop, enum mol_atom_hbond_props, 0x00, UNKNOWN_HPROP);
AG_FIELD(hbond_base_atom_id, size_t, SIZE_MAX, 0);
AG_FIELD(pwpot_id, int, -1, -1);
AG_FIELD(mask, bool, false, false);
AG_FIELD(surface, bool, false, false);  // TODO: Check compatibility with PIPER
AG_FIELD(attraction_level, double, 0, 0);
AG_FIELD(backbone, bool, false, false);
AG_FIELD(ftypen, size_t, SIZE_MAX, 0);
AG_FIELD(ftype_name, char*, NULL, "X");

#endif // _MOL_ATOM_GROUP_MISSING_H_
