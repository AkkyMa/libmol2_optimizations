#ifndef _MOL_BENERGY_H_
#define _MOL_BENERGY_H_

#include "atom_group.h"

/** \file benergy.h
        This file contains  functions
        for calculating bonded energies and forces
	(bonds, angles, dihedrals, impropers)
*/

/**
  find the bond energy and forces
*/
void beng(struct mol_atom_group *ag, double *en);

/**
  find the angle energy and forces
*/
void aeng(struct mol_atom_group *ag, double *en);

/**
  find the improper energy and forces
*/
void ieng(struct mol_atom_group *ag, double *en);

/**
  find the dihedral energy and forces
*/
void teng(struct mol_atom_group *ag, double *en);

#endif
