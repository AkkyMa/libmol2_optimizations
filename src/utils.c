#include "utils.h"
#include "private_utils.h"
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

#ifdef __GLIBC__  /* If we're using glibc, check that it is up-to-date enough to have feenableexcept */
#define __USE_GNU
#include <fenv.h>
#include <features.h>
#define _HAVE_FEENABLEEXECEPTS __GLIBC_PREREQ(2, 2)  // feenableexcept is available since glibc 2.2
#else
/* If we're using something other than glibc, we have no feenableexcept
 * There seem to be methods for enabling FPEs on MacOS: https://stackoverflow.com/a/49102805/929437
 * But since the whole FPE thing is not essential, I'd rather not add any more platform-dependent code. */
#undef _HAVE_FEENABLEEXECEPTS
#endif  /* __GLIBC__ */

int size_t_cmp(const void *a, const void *b)
{
	const size_t aa = *((const size_t *) a);
	const size_t bb = *((const size_t *) b);
	return (aa < bb) ? -1 : (aa > bb);
}

bool is_whitespace_line(const char *line)
{
	size_t i = 0;

	while(line[i] != '\0') {
		char ch = line[i];
		if (!isspace(ch))
			return false;

		++i;
	}

	return true;
}

char *rstrip(char *string)
{
	size_t len = strlen(string);
	char *end = string + len - 1;

	while (end >= string && isspace(*end)) {
		end--;
	}
	*(end + 1) = '\0';

	return string;
}

void mol_enable_floating_point_exceptions(void)
{
#if _HAVE_FEENABLEEXECEPTS
	feenableexcept(FE_DIVBYZERO | FE_INVALID | FE_OVERFLOW);
#ifdef __SANITIZE_ADDRESS__
	_PRINTF_WARNING("%s\n\t%s\n\t%s",
		"Enabling floating point exceptions together with AddressSanitizer.",
		"It may result in ASAN:DEADLYSIGNAL error and program crash whenever FPE is raised.",
		"Most likely, that's okay as long as you don't want to catch the signal yourself.");
#endif  /* __SANITIZE_ADDRESS__ */
#else  /* _HAVE_FEENABLEEXECEPTS */
	_PRINTF_WARNING("CAn not enable floating point exceptions with current build!");
#endif  /* _HAVE_FEENABLEEXECEPTS */
}


#ifdef _WIN32
ssize_t getline2 (char **lineptr, size_t *n, FILE *stream)
{
	char *read = (*lineptr);
	if (read == NULL) {
		read = malloc(10000*sizeof(char));
		if (read == NULL) {
			return -1;
		}
		*lineptr = read;
		*n = 10000;
	}

	char *result = fgets(read, *n, stream);
	if (result == NULL) {
		return -1;
	}

	size_t len = strlen(read);
	while (len == (*n)-1) {
		read = realloc(read, (*n)*2*sizeof(char));
		if (read == NULL) {
			return -1;
		}
		*lineptr = read;
		result = fgets(read+(*n)-1, (*n)+1, stream);
		(*n) *= 2;
		len = strlen(read);
	}
	return len;
}

char *strndup(const char *s, size_t n)
{
	char *out = calloc(n + 1, sizeof(char));
	strncpy(out, s, n);
	return out;
}
#endif /* _WIN32 */
