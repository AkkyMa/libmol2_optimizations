#ifndef _MOL_GEOMETRY_H_
#define _MOL_GEOMETRY_H_

#include <stdlib.h>

/**
   bond struct
   a0---a1

   e = k * (l - l0)^2
*/
struct mol_bond {
	union {
		size_t atom_indices[2];
		struct {
			size_t ai, aj;
		};
	};


	double l;  // bond length
	double l0;  // equilibrium length
	double k;  // spring constant

	int sdf_type;  // sdf type of bond
};

struct mol_bond_list {
	size_t size;
	struct mol_bond **members;
};

/**
   angle struct
   a0
    \
     \
      a1---a2

   e = k * (th - th0)^2
*/
struct mol_angle {
	union {
		size_t atom_indices[3];
		struct {
			size_t a0, a1, a2;
		};
	};

	double th;  // theta
	double th0;  // equilibrium theta
	double k;  // spring constant
};

struct mol_angle_list {
	size_t size;
	struct mol_angle **members;
};

/**
   dihedral struct
   a0         a3
    \         /
     \       /
      a1---a2

   e = k * (1 + cos (n*chi - d))
*/
struct mol_dihedral {
	union {
		size_t atom_indices[4];
		struct {
			size_t a0, a1, a2, a3;
		};
	};

	double chi;  // angle between the planes of a0,a1,a2 and a1,a2,a3
	double k;  // spring constant
	double d;  // delta constant
	size_t n;  // number of bonds made by a1 and a2
};

struct mol_dihedral_list {
	size_t size;
	struct mol_dihedral **members;
};

/**
   improper struct
   a1---a0---a3
   |
   |
   a2

   e = k * (psi - psi0)^2
*/
struct mol_improper {
	union {
		size_t atom_indices[4];
		struct {
			size_t a0, a1, a2, a3;
		};
	};

	double psi;
	double psi0;
	double k;  // spring constant
};

struct mol_improper_list {
	size_t size;
	struct mol_improper **members;
};

struct mol_bond *new_mol_bond(void);
void free_mol_bond(struct mol_bond *b);

struct mol_bond_list *new_mol_bond_list(void);

#endif /* _MOL_GEOMETRY_H_ */
