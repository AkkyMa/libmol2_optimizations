#define _POSIX_C_SOURCE 200809L
#include "geometry.h"
#include "utils.h"

struct mol_bond *new_mol_bond(void)
{
	return calloc(1, sizeof(struct mol_bond));
}

void free_mol_bond(struct mol_bond *b)
{
	if (b != NULL) {
		free(b);
	}
}

struct mol_bond_list *new_mol_bond_list(void)
{
	return calloc(1, sizeof(struct mol_bond_list));
}
