#ifndef _MOL_SEQUENCE_H_
#define _MOL_SEQUENCE_H_

#include <stdio.h>
#include <stdbool.h>

#include "atom_group.h"

char *mol_atom_group_sequence(const struct mol_atom_group *ag);
bool mol_attach_pdb_seqres(struct mol_atom_group *ag, FILE *fp);

#endif /* _MOL_SEQUENCE_H_ */
