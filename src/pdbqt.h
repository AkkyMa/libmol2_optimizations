#ifndef _MOL2_PDBQT_H_
#define _MOL2_PDBQT_H_

#define MOL_PDBQT_ATOM_TYPE_METADATA_KEY "autodock_type"

#include "atom_group.h"

enum mol_autodock_atom_type {
	AUTODOCK_ATOM_C = 0,  ///< Carbon (non-acceptor)
	AUTODOCK_ATOM_A = 1,  ///< Carbon (acceptor)
	AUTODOCK_ATOM_N = 2,  ///< Nitrogen (neither donor not acceptor)
	AUTODOCK_ATOM_O = 3,  ///< Oxygen (neither donor not acceptor)
	AUTODOCK_ATOM_P = 4,  ///< Phosphorus
	AUTODOCK_ATOM_S = 5,  ///< Sulfur
	AUTODOCK_ATOM_H = 6,  ///< Hydrogen (non-donatable)
	AUTODOCK_ATOM_F = 7,  ///< Fluoride
	AUTODOCK_ATOM_I = 8,  ///< Iodine
	AUTODOCK_ATOM_NA = 9,  ///< Nitrogen (acceptor)
	AUTODOCK_ATOM_OA = 10,  ///< Oxygen (acceptor)
	AUTODOCK_ATOM_SA = 11,  ///< Sulfur (acceptor)
	AUTODOCK_ATOM_HD = 12,  ///< Hydrogen (donatable)
	AUTODOCK_ATOM_Mg = 13,  ///< Magnesium
	AUTODOCK_ATOM_Mn = 14,  ///< Manganese
	AUTODOCK_ATOM_Zn = 15,  ///< Zinc
	AUTODOCK_ATOM_Ca = 16,  ///< Calcium
	AUTODOCK_ATOM_Fe = 17,  ///< Iron
	AUTODOCK_ATOM_Cl = 18,  ///< Chlorine
	AUTODOCK_ATOM_Br = 19,  ///< Bromine
	AUTODOCK_ATOM_X = 20,  ///< Unknown atom type
};


/**
 * Read protein structure from Autodock PDBQT file.
 *
 * Only atom information is read, the tree structure is ignored for now.
 * The Autodock atom types are stored as metadata with \c MOL_PDBQT_ATOM_TYPE_METADATA_KEY key.
 *
 * \param pdbqt_filename The name of file to read.
 * \return New \ref mol_atom_group, or \c NULL on error.
 */
struct mol_atom_group *mol_pdbqt_read(const char *pdbqt_filename);

/**
 * Deduce AutoDock atom types based on atom elements and donor/acceptor status.
 *
 * Atomgroup must have H-bond properties initialized (typically, loaded from appropriate PSF or JSON topology file).
 * The Autodock atom types are stored as metadata with \c MOL_PDBQT_ATOM_TYPE_METADATA_KEY key.
 *
 * The atom types are determined based on element and its donor/acceptor status (see \ref mol_autodock_atom_type).
 * If the element name has more than 2 letters, a warning is printed (while atom types can be up to 4-letter long,
 * it's unlikely to encounter any element from the end of the periodic table in biological systems).
 * If the atom can not be mapped to any of AutoDock types, it is assigned \ref AUTODOCK_ATOM_X type. Function still
 * returns \c true, if no other errors occured.
 *
 * \param ag Atomgroup.
 * \return True on success, false on failure. If failed, no metadata is added.
 */
bool mol_pdbqt_guess_atom_types(struct mol_atom_group *ag);

#endif // _MOL2_PDBQT_H_
