if(NOT EMBED_LIBMOL)
  cmake_minimum_required(VERSION 3.0)
endif()
if(CMAKE_VERSION VERSION_GREATER 3.9)
  cmake_policy(SET CMP0069 NEW)
endif()

#bump minor version for api changes
set(libmol_version_major 2)
set(libmol_version_minor 2)
set(libmol_version_patch 0)
set(libmol_version
  "${libmol_version_major}.${libmol_version_minor}.${libmol_version_patch}")

project(mol2
  LANGUAGES C
  VERSION ${libmol_version})
set(CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/cmake/")

find_package(Git)
if(GIT_FOUND AND EXISTS "${CMAKE_SOURCE_DIR}/.git")
  execute_process(
    COMMAND git describe --always HEAD
    COMMAND sed -e "s/-/./g"
    OUTPUT_VARIABLE GIT_VERSION
    OUTPUT_STRIP_TRAILING_WHITESPACE)
else()
  set(GIT_VERSION "git")
endif()

find_package(Gperf REQUIRED)

find_package(Jansson REQUIRED)

# build options
option( SONAME				    "Set the (SO)VERSION of the target"		ON  )
option( BUILD_SHARED_LIBS	"Build Shared Library (OFF for Static)"	OFF )
option( PROFILE				    "Generate profiling information"		OFF )
option( BUILD_TESTS			  "Build tests"							OFF )
option( USE_LTO			      "Use Link Time Optimization"							ON )
option( USE_COVERAGE      "Enable unit test code coverage" OFF)
option( USE_SANITIZER		  "Build with address sanitizer"		OFF )

if (CMAKE_INSTALL_PREFIX_INITIALIZED_TO_DEFAULT AND UNIX)
  set( CMAKE_INSTALL_PREFIX $ENV{HOME} CACHE PATH "Base directory for installation" FORCE)
endif()

add_definitions(
  -D _MOL_VERSION_="\\"${libmol_version}\\""
  -D _GIT_VERSION_="\\"${GIT_VERSION}\\"")

if (MSVC)
  add_definitions(
    -D _CRT_SECURE_NO_WARNINGS
    -D _CRT_NONSTDC_NO_DEPRECATE
  )
endif(MSVC)

# Compile flags
if (BUILD_SHARED_LIBS)
  set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -fvisibility=hidden -fPIC")
endif ()

if(PROFILE)
  set(CMAKE_C_FLAGS "-pg ${CMAKE_C_FLAGS}")
  set(CMAKE_EXE_LINKER_FLAGS "-pg ${CMAKE_EXE_LINKER_FLAGS}")
endif()

if (UNIX)
  set(CMAKE_C_FLAGS
    "${CMAKE_C_FLAGS} -std=c11")
  set(CMAKE_C_FLAGS_DEBUG
    "${CMAKE_C_FLAGS_DEBUG} -Wall -Wshadow -Wpointer-arith -Wcast-qual -Winline -Werror -Wextra -Wfatal-errors -Wstrict-prototypes")
endif()

if(USE_COVERAGE)
  set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -O0 -fprofile-arcs -ftest-coverage")
endif()

if(USE_SANITIZER)
  set(SANITIZER_FLAGS "-fno-omit-frame-pointer -fsanitize=address,undefined")
  set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${SANITIZER_FLAGS}")
  set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS}  ${SANITIZER_FLAGS}")
endif()


# Add __FILENAME__ macro
function(define_file_basename_for_sources targetname)
  get_target_property(source_files "${targetname}" SOURCES)
  foreach(sourcefile ${source_files})
    # Get source file's current list of compile definitions.
    get_property(defs SOURCE "${sourcefile}"
      PROPERTY COMPILE_DEFINITIONS)
    # Add the FILE_BASENAME=filename compile definition to the list.
    get_filename_component(basename "${sourcefile}" NAME)
    list(APPEND defs "__FILENAME__=\"${basename}\"")
    # Set the updated compile definitions on the source file.
    set_property(
      SOURCE "${sourcefile}"
      PROPERTY COMPILE_DEFINITIONS ${defs})
  endforeach()
endfunction()


# libmol2
# set source files
add_custom_command(OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/sequence.c
	COMMAND ${GPERF_EXECUTABLE} --output-file=${CMAKE_CURRENT_BINARY_DIR}/sequence.c -m 20 ${CMAKE_SOURCE_DIR}/src/sequence.gperf
	DEPENDS ${CMAKE_SOURCE_DIR}/src/sequence.gperf
  )
file(GLOB LIBMOL_SOURCES src/*.c)
list(APPEND LIBMOL_SOURCES
  ${CMAKE_CURRENT_BINARY_DIR}/sequence.c
)
if (UNIX)
  set_source_files_properties(${CMAKE_CURRENT_BINARY_DIR}/sequence.c
    PROPERTIES COMPILE_FLAGS "-Wno-unused-parameter -Wno-missing-field-initializers")
  set_source_files_properties("src/lbfgs.c"
    PROPERTIES COMPILE_FLAGS "-Wno-unused-parameter")
endif()
file(GLOB MOL2_PUBLIC_HEADERS src/*.h)
file(GLOB ARITHMETIC_HEADERS src/arithmetic*.h)
list(REMOVE_ITEM MOL2_PUBLIC_HEADERS ${ARITHMETIC_HEADERS})

set(LIB_INSTALL_DIR "lib" CACHE PATH "Where to install libraries to.")
set(CONFIG_INSTALL_DIR "lib/cmake/${PROJECT_NAME}" CACHE PATH "Where to install configs to.")
set(INCLUDE_INSTALL_DIR "include" CACHE PATH "Where to install headers to.")

add_library(mol${libmol_version_major} ${LIBMOL_SOURCES})
if(NOT MSVC)
  target_link_libraries(mol${libmol_version_major}
    ${JANSSON_LIBRARIES} m)
else()
  target_link_libraries(mol${libmol_version_major}
    ${JANSSON_LIBRARIES})
endif()
target_include_directories(mol${libmol_version_major}
  PUBLIC
  "$<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/src>"
  "$<INSTALL_INTERFACE:${INCLUDE_INSTALL_DIR}>"
  PRIVATE ${JANSSON_INCLUDE_DIRS})
set_target_properties(mol${libmol_version_major} PROPERTIES PUBLIC_HEADER "${MOL2_PUBLIC_HEADERS}")
set_target_properties(mol${libmol_version_major} PROPERTIES VERSION "${libmol_version}")
define_file_basename_for_sources(mol${libmol_version_major})


if(NOT CMAKE_BUILD_TYPE STREQUAL "Debug" AND USE_LTO AND CMAKE_VERSION VERSION_GREATER 3.9)
  include(CheckIPOSupported)
  check_ipo_supported(RESULT result)
  if(result)
    set_target_properties(mol${libmol_version_major} PROPERTIES INTERPROCEDURAL_OPTIMIZATION True)
  endif()
endif()

include(CMakePackageConfigHelpers)
write_basic_package_version_file(${CMAKE_CURRENT_BINARY_DIR}/mol2/mol2ConfigVersion.cmake
  VERSION ${libmol_version}
  COMPATIBILITY AnyNewerVersion)

configure_package_config_file(cmake/mol2Config.cmake.in
  ${CMAKE_CURRENT_BINARY_DIR}/mol2/mol2Config.cmake
  INSTALL_DESTINATION "${CONFIG_INSTALL_DIR}")

install(TARGETS "mol${libmol_version_major}"
  EXPORT mol2Targets
  LIBRARY DESTINATION "${LIB_INSTALL_DIR}"
  ARCHIVE DESTINATION "${LIB_INSTALL_DIR}"
  PUBLIC_HEADER DESTINATION "${INCLUDE_INSTALL_DIR}/mol${libmol_version_major}"
)

install(FILES
  ${CMAKE_CURRENT_BINARY_DIR}/mol2/mol2Config.cmake
  ${CMAKE_CURRENT_BINARY_DIR}/mol2/mol2ConfigVersion.cmake
  DESTINATION ${CONFIG_INSTALL_DIR})

install(
  EXPORT mol2Targets
  FILE mol2Targets.cmake
  DESTINATION ${CONFIG_INSTALL_DIR})

# Documentation
# TODO

# Testing
if(BUILD_TESTS)
  find_package(Check)
  if(CHECK_FOUND)
    enable_testing()
    add_subdirectory(tests)
    if(USE_COVERAGE)
      include(CodeCoverage)
      SETUP_TARGET_FOR_COVERAGE(coverage "make test" code_coverage)
    endif()
  else()
    message("Could not find Check for testing")
  endif()
endif()
