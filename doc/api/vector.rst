.. include:: ../global.rst

mol_vector3
===========

Since most of the time we are dealing with atomic data, it made sense
to have a structure to represent a point in space. Along with these
structure definitions come a large number of useful macros for adding,
multiplying, dot/cross product-ing, etc.

.. type:: struct mol_vector3
          struct mol_vector3f
          struct mol_vector3l
          struct mol_vector3i
          struct mol_vector3u

   .. member:: double X
               double Y
               double Z

Most macros defined here will work on anything (like :type:`mol_quaternion`)
that has the fields ``X``, ``Y``, and ``Z``.

Macros are not "safe", as in if the expression provided for any of the arguments
have side-effects, the side-effects will happen 3 times. To avoid this, do not
use a function call as one of the input expressions. If you need to use the output of
a function as an argument, assign it to a temporary variable first.

.. doxygenfile:: vector.h
