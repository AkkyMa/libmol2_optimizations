.. include:: ../global.rst

Utils
=====

Collection of various useful functions used throughout the library.


.. doxygenfile:: utils.h
