.. include:: ../global.rst

mol_atom_group
==============

.. doxygenstruct:: mol_atom_group
    :members:
    :undoc-members:
    :no-link:

.. doxygenfile:: atom_group.h
