.. include:: ../global.rst

Rmsd
====

This library solely focuses on providing root-mean-square distances of proteins
given a variety of different data structure representations.


.. doxygenfile:: rmsd.h
