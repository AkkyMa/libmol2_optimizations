.. include:: ../global.rst

Transform
=========

This library is used for spatial transformations of proteins in 3D space. This
functionality stems from the fact that each of the proteins can be expressed as
mol_vector3 structs, so we take advantage of Linear Algebraic manipulations to
transform the proteins in 3D space. Each of these functions provides the ability
to spatially manipulate either a single protein, or a list of proteins.

Functions
---------

The following functions are used to spatially adjust the protein structures. For
reference, it will be helpful to read over the documentation for the structure
types we have defined. The link is posted below:

.. toctree::
   :maxdepth: 1

   vector

The functions are logically grouped according to type; we have an identical
transformation function for doubles, floats and longs(labeled as '3', '3f', and
'3l' respectively), and also have identical movement functions that operate on
lists of protein structures of types double, float and long.

.. function:: void mol_vector3_translate(struct mol_vector_3 *v,const struct mol_vector3 *tv)
              void mol_vector3f_translate(struct mol_vector_3f *v, const struct mol_vector3f *tv)
              void mol_vector3l_translate(struct mol_vector_3l *v, const struct mol_vector3l *tv)

    This function group takes in a pointer to a 3D vector ``v`` which contains 3D
    coordinates of an atom, as well as another mol_vector pointer ``tv``. This
    outputs translated x, y and z coordinates for ``v``. If invalid input is
    entered, specifically a null pointer, then the function sets errno = EFAULT and
    returns without changing ``v``.
    Each of the functions above translates the ``v`` vector in 3D space by the
    values designated in the ``tv`` vector (for doubles, floats and longs
    respectively). Given the fact that the mol_vector structure represents an atom
    in 3D space, this will allow for the translational movement of atomic points in
    3D space. The function does so through the aforementioned ``MOL_VEC_ADD`` macro.

.. function:: void mol_vector3_rotate(struct mol_vector3 *v, const struct mol_matrix3 *r)
              void mol_vector3f_rotate(struct mol_vector3f *v, const struct mol_matrix3f *r)
              void mol_vector3l_rotate(struct mol_vector3l *v, const struct mol_matrix3l *r)

    This function group takes in a pointer to a 3D mol_vector3 ``v`` which contains
    3D coordinates of an atom. The group also takes in a 3x3 matrix pointer ``r``.
    The output is a rotated vector ``v`` specified by thevalues in ``r``. If invalid
    input is entered, specifically a null pointer, the function sets errno = EFAULT
    and returns without performing any computation.
    Each of the functions above rotates the vector in 3D space. It does so through
    the inputted 3x3 rotation ``tv`` matrix, which rotates the ``v`` argument
    pointer about the R3 coordinate system around the specified origin. Like above,
    it does so for doubles, floats and longs, and also has null pointer argument
    protection.

.. function:: void mol_vector3_move(struct mol_vector3 *v, const struct mol_matrix3 *r, const struct mol_vector_3f *tv)
              void mol_vector3f_move(struct mol_vector3f *v, const struct mol_matrix3f *r, const struct mol_vector_3f *tv)
              void mol_vector3l_move(struct mol_vector3l *v, const struct mol_matrix3l *r, const struct mol_vector_3l *tv)

    This group of functions takes in a pointer to a 3D vector ``v``, as well as a
    3x3 rotation matrix pointer ``r`` and another 3D vector pointer ``tv``. The
    output should be rotated and translated coordinates of ``v``. If a null pointer
    is passed, the function sets errno=EFAULT and returns without altering ``v``.
    Each of the functions above combine the functionality of the previous two sets
    of functions above. It first starts by performing a rotation in R3 of ``v``
    according to the 3x3 rotation matrix ``r``, and then translates ``v`` by the
    vector ``tv``. Thus, it first does a 'rotate', then a 'translate', combining the
    functionality above. Again, we still retain the three type compatibility, and
    null pointer protection.

.. function:: void mol_vector3_translate_list(struct mol_vector3 *v_list, size_t len, const struct mol_vector3 *tv)
              void mol_vector3f_translate_list(struct mol_vector3f *v_list, size_t len, const struct mol_vector3f *tv)
              void mol_vector3l_translate_list(struct mol_vector3l *v_list, size_t len, const struct mol_vector3l *tv)

    This group of functions takes in a pointer to a list of 3D vectors, ``v_list``.
    It also takes in the number of vectors in this list in ``len``, as well as a
    pointer to a 3D vector ``tv``. The output should be translated coordinates of
    all of the vectors in ``v_list``, and if an invalid null pointer is passed the
    function sets errno=EFAULT and returns without performing any computation.
    These list functions apply the same translate function described above, but
    instead take in ``len`` vectors from ``v_list`` and translate each of those
    vectors in ``v_list`` by ``tv``.

.. function:: void mol_vector3_rotate_list(struct mol_vector3 *v_list, size_t len, const struct mol_matrix3 *r)
              void mol_vector3f_rotate_list(struct mol_vector3f *v_list, size_t len, const struct mol_matrix3f *r)
              void mol_vector3l_rotate_list(struct mol_vector3l *v_list, size_t len, const struct mol_matrix3l *r)

    This group of functions takes in a pointer to a list of 3D vectors ``v_list``,
    which contains ``len`` vectors, as well as a 3x3 rotation matrix ``r``. The
    output will consist of all of the vectors rotated in ``v_list``, and if a null
    pointer, invalid argument is entered the function will exit without performing
    any computation.
    These rotate functions apply the same rotation described above, but take ``len``
    vectors in ``v_list`` and rotate each of those vectors in ``v_list`` in R3 by
    the rotation matrix ``r``.

.. function:: void mol_vector3_move_list(struct mol_vector3 *v_list, size_t len, const struct mol_matrix3 *r, const struct mol_vector3 *tv)
              void mol_vector3f_move__list(struct mol_vector3f *v_list, size_t len, const struct mol_matrix3f *r, const struct mol_vector3f *tv)
              void mol_vector3l_move_list(struct mol_vector3l *v_list, size_t len, const struct mol_matrix3l *r, const struct mol_vector3l *tv)

    This group of functions takes in a pointer to a list of 3D vectors ``v_list``
    with ``len`` vectors. It also takes in a pointer to a 3x3 rotation matrix ``r``,
    as well as another 3D vector pointer for translation ``tv``. The output should
    be a rotated and translated set of vectors in ``v_list``, and if any of these
    arguments are null pointers the function will set errno=EFAULT and return
    without performing any computation.
    These move functions work in the same way as the aforementioned move function,
    but operate on the ``v_list`` of ``len`` vectors, and rotates each vector by the
    rotation matrix ``r``, and then translates them by ``tv``

.. c:function:: bool mol_get_transform_to_residue(	struct mol_vector3 *translation, struct mol_matrix3 *rotation, \\
            const struct mol_atom_group *ag, const struct mol_residue *residue)

   Obtains the translation and rotation, then when applied, will move a residue
   in the "canonical" position to the position to align with the specified
   residue in the atom group. The canonical position is when the N, CA, and C
   atoms all lie in the `z=0` plane, and the CA atom has coordinate `0,0,0`, and
   the N atom lies on the `y=0,z=0` line.

   :param translation: pointer to valid location to output the translation
   :param rotation: pointer to valid location to output the rotation
   :param ag: pointer to atom group
   :param residue: pointer to a residue returned from ``find_residue``

   :return: True on success, false otherwise
