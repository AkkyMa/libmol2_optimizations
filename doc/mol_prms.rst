Internal Paramters
==================

The lab internal parameter files are read by functions in
``prms.h``. This step is now separate from reading in structures from
PDB files. The internal parameters include Van Der Waals radii,
charge, and DARS pairwise potentials.
