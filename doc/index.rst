.. include:: global.rst

.. libmol documentation master file, created by
   sphinx-quickstart on Thu Nov 13 18:00:34 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to libmol's documentation!
==================================

Introduction
------------

libmol is a lightweight collection of basic data structures and
functions for doing molecular modelling. In particular, libmol was
developed for the docking program PIPER, and many of the design
decisions are to optimize for that use case.

.. toctree::
   :maxdepth: 2

   tutorial
   api

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
