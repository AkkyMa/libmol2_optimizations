Tutorial
========

This tutorial will go through how to obtain and setup libmol for use
in any projects which require it.

Setup
-----

First, make sure you have all the dependencies installed. libmol
depends on CMake (specified as v2.8 or greater, but will probably work
will lower versions), and a C compiler (tested with gcc, may work with
clang).

Assuming you have checked out the libmol repository using git, from
the root directory run the run the following commands:

.. code-block:: sh

  mkdir build
  cd build
  cmake -DCMAKE_BUILD_TYPE=RelWithDebInfo ..
  make && make install

This should install libmol into the default location
(``$HOME/include`` and ``$HOME/lib``).
