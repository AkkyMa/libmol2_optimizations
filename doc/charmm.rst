.. include:: global.rst

CHARMM
======

CHARMM parameter files are used by functions in ``icharmm.h`` to fill
in bond, angle, dihedral, and impropers in atom groups.

PSF files
---------

PSF (protein structure file) contains molecule-specific information necessary for calculating forcefield: atom types,
charges, and masses, bonds, andgles, dihedrals and impropers, donors and acceptors.

PSF is a positional (fixed width) file format. It is organized in sections, each section starting with header containing
number of records and section name, followed by corresponding number of records. All sections
except ``NATOM`` contain flat stream of atom numbers.

For example, this section::

           4 !NTHETA: angles
           1       3       4       1       3       8       3       8       9
           3       8      10

means that the molecule has 4 angles, formed by atoms (1, 3, 4), (1, 3, 8), (3, 8, 9), (3, 8, 10).

Hydrogen bonds
^^^^^^^^^^^^^^

The atoms that can partake in hydrogen bonding may be listed in sections ``NDON`` and ``NACC``.
These sections can be omitted unless you wish to use hbond potential.

In CHARMM forcefields, four atoms are involved in hydrogen bonds: donor (*D*), donatable hydrogen (*H*), acceptor (*A*), heavy atom covalently bound to acceptor (*AA*) [#fhydroaa]_.

The ``NDON`` section contains pairs of atom indexes: ``D H``. For atom *H* the base atom is the corresponding *D*.

The ``NACC`` section also contains pairs of indexes: ``A AA``. For atom *A* the base atom is the corresponding *AA*.

For atoms that are neither *H* nor *A* the notion of base (stored in :c:member:`hbond_base_atom_id` of :c:type:`mol_atom_group`) is meaningless.

.. rubric:: Footnotes

.. [#fhydroaa] In some forcefields (not CHARMM) the AA atom is a virtual atom, but PSF format assumes that it is a physical one.
