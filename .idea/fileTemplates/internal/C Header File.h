#parse("IncludeGuard.h")##
#set( $blank = "" )
#parse("C File Header.h")
#[[#ifndef]]# #includeGuard(${NAME} "h")${blank}
#[[#define]]# #includeGuard(${NAME} "h")${blank}

#[[#endif]]# // #includeGuard(${NAME} "h")
